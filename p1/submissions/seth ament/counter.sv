module counter(input clk, load, input [9:0] data, output reg tc);

	reg [9:0] count;
	always @(posedge clk) begin
		if (load) begin
			count = data;
		end else begin
			// Check if the counter has reached 0.
			if (count == 0) tc <= 1;
			else count = count - 1;
		end // end else
	end // always @(posedge clk)

endmodule

module counter_tb;
	logic clk, load;
	logic [9:0] data;
	logic tc;

	counter dut(.clk(clk), .load(load), .data(data), .tc(tc));

	initial begin
		clk = 0; forever #5 clk = ~clk;
	end

	initial begin
		data = 3; load = 1;
		#10 load = 0;
		$monitor("t=%2d tc=%1b", $time, tc);
		#40 data = 2; load = 1;
		#10 load = 0;
		#60 $stop;
	end

	initial $dumpvars(0, counter_tb);

endmodule

module gshare(output predicted, input [n-1:0] gh, pc,
	input actual, update, reset, clk);
	parameter n = 4;
	logic [1:0] counter [0:2**n-1];

	integer i;
	integer index;

	assign predicted = counter[index] >> 1;

	always @(posedge reset) begin
		index <= 0;
		for (i=0; i<2**n-1; i=i+1) counter[i] <= 1;
	end

	always @(posedge clk) begin
		if (update) begin
			index = pc ^ gh;
			if (actual && counter[index] != 3) begin
				counter[index] = counter[index] + 1;
			end else if (!actual && counter[index] != 0) begin
				counter[index] = counter[index] - 1;
			end
		end
	end

endmodule


/* testbench module */
module gshare_tb;
	parameter m=2;
	logic [31:0] pc;
	logic [m-1:0] gr;
	logic actual, update, reset, clk;
	logic predicted;

	gshare #(.n(m)) dut(predicted, pc[m-1:0], gr, actual, update, reset, clk);

	initial begin
		clk = 0; forever #5 clk = ~clk;
	end

	initial $monitor("t=%2d: counter[0] %2b counter[1] %2b", $time,
		dut.counter[0], dut.counter[1]);
	initial begin
	reset = 1; update = 0; pc = 0; gr = 0;
	# 10 reset = 0;
	# 10 actual = 1; update = 1;
	# 10 actual = 1;
	# 10 actual = 0;
	# 10 actual = 0;
	# 10 actual = 0;
	# 10 gr = 1; update = 0;
	# 10 actual = 1; update = 1;
	# 10 actual = 1; update = 1;
	# 10 actual = 1; update = 1;
	# 10 update = 0;
	# 10 $stop;
	end

	initial $dumpvars(0, gshare_tb);

endmodule

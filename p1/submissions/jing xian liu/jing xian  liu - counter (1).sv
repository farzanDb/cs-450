module counter(input clk, load, input [9:0] data, output tc);

	reg [9:0] count;
	logic tc;

	initial begin
		tc = 0;
	end
	
	always@ (posedge clk) begin
		if (load == 1) begin
			count = data;
			tc = 0;
		end 
		else begin
			count = count - 1;
			if (count == 0) begin
				tc = 1;
			end  
		end
	end

endmodule

module counter_tb;
	logic clk, load;
	logic [9:0] data;
	logic tc;

	counter dut(.clk(clk), .load(load), .data(data), .tc(tc));

	initial begin
		clk = 0; forever #5 clk = ~clk;
	end

	initial begin
		data = 3; load = 1;
		#10 load = 0;
		$monitor("t=%2d tc=%1b", $time, tc);
		#40 data = 2; load = 1;
		#10 load = 0;
		#60 $stop;
	end

	initial $dumpvars(0, counter_tb);

endmodule

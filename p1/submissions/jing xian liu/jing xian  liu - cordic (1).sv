module cordic(input clk, go, input [15:0] a,
	output logic done, output logic [15:0] sin, cos);

	logic [15:0] e [0:15];

	logic signed [15:0] x_0 = 16'h26dd;
	logic [15:0] x_hold = 16'h26dd;

	logic signed [15:0] x, y, z;
	reg start;
	integer i;
	logic done_local;


	initial begin
		start = 0;
		e[0] = 16'h3244;
		e[1] = 16'h1dac;
		e[2] = 16'h0fae;
		e[3] = 16'h07f5;
		e[4] = 16'h03ff;
		e[5] = 16'h0200;
		e[6] = 16'h0100;
		e[7] = 16'h0080;
		e[8] = 16'h0040;
		e[9] = 16'h0020;
		e[10] = 16'h0010;
		e[11] = 16'h0008;
		e[12] = 16'h0004;
		e[13] = 16'h0002;
		e[14] = 16'h0001;
		e[15] = 16'h0000;
	end

	always@(posedge clk or go) begin 
		if (go == 0 && start == 0) begin
			start = 1;
			z = a;
			y = 0;
			x = x_hold;
			done_local = 0;
            i = 0;
		end
		if (start == 1) begin 
            if (i < 16) begin 
				if (z >= 0) begin 
					z = z - e[i];
					x_0 = x;
					x = x - $signed(y >>> i);
					y = y + $signed(x_0 >>> i);
				end 
				else begin
					z = z + e[i];
					x_0 = x;
					x = x + $signed(y >>> i);
					y = y - $signed(x_0 >>> i);
				end 
                i = i + 1;
			end
            else begin
                start = 0;
			    done_local = 1;
                i = 0;
            end
		
		end 

		cos = x;
		sin = y;
		done = done_local;
		done_local = 0;


	end

endmodule

module cordic_tb;
	logic clk, go, done;
	logic [15:0] a, sin, cos;
	cordic dut(.clk(clk), .go(go), .a(a), .done(done), .sin(sin), .cos(cos));

	initial begin
		clk = 0; forever #5 clk = ~clk;
	end

	initial begin
		a = 16'h0; // angle = 0 rad
		go = 1;
		#10 go = 0;
		wait(done==1)
			// should display roughly sin=0000 and cos=4000 in hexadecimal
			$display("t=%3d sin=%4h cos=%4h", $time, sin, cos);

		#10 a = 16'h4000; // angle = 1 rad
		go = 1;
		#10 go = 0;
		wait(done==1)
			// should display roughly sin=35da and cos=2294 in hexadecimal
			$display("t=%3d sin=%4h cos=%4h", $time, sin, cos);

		#10 $stop;
	end

	initial $dumpvars(0,cordic_tb);

endmodule
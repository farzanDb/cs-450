module gshare(output predicted, input [n-1:0] gh, pc,
	input actual, update, reset, clk);
	parameter n = 4;
	logic [1:0] counter [0:2**n-1];

	// your code here
	integer i;
	logic [n-1:0] c; 
	logic predicted;
	
	initial begin
		for (i = 0; i <= 2**n-1; i++) begin 
				counter[i] = 2'b01;
		end
	end

	always@(posedge clk) begin
		if (reset == 1) begin
			for (i = 0; i <= 2**n-1; i++) begin 
				counter[i] = 2'b01;
			end
		end
		if (update == 1) begin
			c = pc ^ gh;
			case(counter[c])
				2'b00: if (actual == 1) begin
					counter[c] = 2'b01;
				end
				2'b01: if (actual == 1) begin
					counter[c] = 2'b10;
				end
				else begin
					counter[c] = 2'b00;
				end
				2'b10: if (actual == 1) begin
					counter[c] = 2'b11;
				end
				else begin
					counter[c] = 2'b01;
				end
				2'b11: if (actual != 1) begin
					counter[c] = 2'b10;
				end
			endcase
			predicted = counter[c][1];

		end
	end

endmodule


/* testbench module */
module gshare_tb;
	parameter m=1;
	logic [31:0] pc;
	logic [m-1:0] gr;
	logic actual, update, reset, clk;
	logic predicted;

	gshare #(.n(m)) dut(predicted, pc[m-1:0], gr, actual, update, reset, clk);

	initial begin
		clk = 0; forever #5 clk = ~clk;
	end

	initial $monitor("t=%2d: counter[0] %2b counter[1] %2b", $time,
		dut.counter[0], dut.counter[1]);

	initial begin
	reset = 1; update = 0; pc = 0; gr = 0;
	# 10 reset = 0;
	# 10 actual = 1; update = 1;
	# 10 actual = 1;
	# 10 actual = 0;
	# 10 actual = 0;
	# 10 actual = 0;
	# 10 gr = 1; update = 0;
	# 10 actual = 1; update = 1;
	# 10 actual = 1; update = 1;
	# 10 actual = 1; update = 1;
	# 10 update = 0;
	# 10 $stop;
	end

	initial $dumpvars(0, gshare_tb);

endmodule

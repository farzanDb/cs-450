module gshare(output predicted, input [n-1:0] gh, pc,
	input actual, update, reset, clk);
	parameter n = 4;
	logic [1:0] counter [0:2**n-1];

	logic [n:0] i;
	reg predicted;

	always @(posedge reset) begin
		for (i = 0; i < 2**n; i = i + 1) begin
			counter[i] = 2'b01;
		end
	end

	always @(posedge clk) begin
		predicted <= counter[gh ^ pc][1];
		if (update) begin
			if (actual) begin
				if (counter[gh ^ pc] < 3) begin
					counter[gh ^ pc] <= counter[gh ^ pc] + 1;
				end
			end else begin
				if (counter[gh ^ pc] > 0) begin
					counter[gh ^ pc] <= counter[gh ^ pc] - 1;
				end
			end
		end
		if (reset) begin
			for (i = 0; i < 2**n; i = i + 1) begin
				counter[i] = 2'b01;
			end
		end
	end

endmodule


/* testbench module */
module gshare_tb;
	parameter m=1;
	logic [31:0] pc;
	logic [m-1:0] gr;
	logic actual, update, reset, clk;
	logic predicted;

	gshare #(.n(m)) dut(predicted, pc[m-1:0], gr, actual, update, reset, clk);

	initial begin
		clk = 0; forever #5 clk = ~clk;
	end

	initial $monitor("t=%2d: counter[0] %2b counter[1] %2b", $time,
		dut.counter[0], dut.counter[1]);
	initial begin
	reset = 1; update = 0; pc = 0; gr = 0;
	# 10 reset = 0;
	# 10 actual = 1; update = 1;
	# 10 actual = 1;
	# 10 actual = 0;
	# 10 actual = 0;
	# 10 actual = 0;
	# 10 gr = 1; update = 0;
	# 10 actual = 1; update = 1;
	# 10 actual = 1; update = 1;
	# 10 actual = 1; update = 1;
	# 10 update = 0;
	# 10 $stop;
	end

	initial $dumpvars(0, gshare_tb);

endmodule

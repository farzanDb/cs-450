module counter(input clk, load, input [9:0] data, output tc);
    logic tc;
    parameter count = 0;

    always @(posedge clk) begin
        if (count == 0) begin
            // set tc to 1
            tc = 1;
        end else if (load == 1) begin
            // load the count value from data input
            count = data;
        end else if (load == 0) begin
            // count down from loaded value towards zero
            count = count - 1;
        end
    end
endmodule

module counter_tb;
	logic clk, load;
	logic [9:0] data;
	logic tc;

	counter dut(.clk(clk), .load(load), .data(data), .tc(tc));

	initial begin
		clk = 0; forever #5 clk = ~clk;
	end

	initial begin
		data = 3; load = 1;
		#10 load = 0;
		$monitor("t=%2d tc=%1b", $time, tc);
		#40 data = 2; load = 1;
		#10 load = 0;
		#60 $stop;
	end

	initial $dumpvars(0, counter_tb);

endmodule

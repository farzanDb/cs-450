module gshare(output predicted, input [n-1:0] gh, pc,
	input actual, update, reset, clk);
	parameter n = 4;
	logic [1:0] counter [0:2**n-1];
	logic predict_reg;
	logic [2:0] state;
	logic [2:0] new_state;
	logic [1:0] set_new_state;

	logic [2:0] SNT = 2'b00;
	logic [2:0] LNT = 2'b01;
	logic [2:0] LT = 2'b10;
	logic [2:0] ST = 2'b11;

	logic [1:0] BT = 1'b1;
	logic [1:0] BNT = 1'b0;

	assign state = counter[gh ^ pc];
	assign predicted = predict_reg;

	initial begin
		set_new_state = 1;
	end

	always @(posedge reset) begin
		foreach(counter[i]) begin
			counter[i] <= 1;
		end
	end

	always @(pc, gh, n, counter[gh ^ pc]) begin
		predict_reg = counter[gh ^ pc][1];  
	end

	always @(posedge clk) begin
		if (update == 1)  begin
			if (state == SNT) begin
				if (actual == BT) begin
					new_state <= LNT;
				end else begin
					new_state <= SNT;
				end
			end else if (state == LNT) begin
				if (actual == BT) begin
					new_state <= LT;
				end else begin
					new_state <= SNT;
				end
			end else if (state == LT) begin
				if (actual == BT) begin
					new_state <= ST;
				end else begin
					new_state <= LNT;
				end
			end else if (state == ST) begin
				if (actual == BT) begin
					new_state <= ST;
				end else begin
					new_state <= LT;
				end
			end
		end	else if (set_new_state == 1) begin
			new_state <= state;
			set_new_state = 0;
		end
	end

	always @(new_state) begin 
			counter[gh ^ pc] = new_state;
	end

endmodule



/* testbench module */
module gshare_tb;
	parameter m=1;
	logic [31:0] pc;
	logic [m-1:0] gr;
	logic actual, update, reset, clk;
	logic predicted;

	gshare #(.n(m)) dut(predicted, pc[m-1:0], gr, actual, update, reset, clk);

	initial begin
		clk = 0; forever #5 clk = ~clk;
	end

	initial $monitor("t=%2d: counter[0] %2b counter[1] %2b", $time,
		dut.counter[0], dut.counter[1]);
	initial begin
	reset = 1; update = 0; pc = 0; gr = 0;
	# 10 reset = 0;
	# 10 actual = 1; update = 1;
	# 10 actual = 1;
	# 10 actual = 0;
	# 10 actual = 0;
	# 10 actual = 0;
	# 10 gr = 1; update = 0;
	# 10 actual = 1; update = 1;
	# 10 actual = 1; update = 1;
	# 10 actual = 1; update = 1;
	# 10 update = 0;
	# 10 $stop;
	end

	initial $dumpvars(0, gshare_tb);

endmodule



// module gshare(output predicted, input [n-1:0] gh, pc,
// 	input actual, update, reset, clk);
// 	parameter n = 4;
// 	logic [1:0] counter [0:2**n-1];
// 	logic predict_reg;
// 	logic [2:0] state;
// 	logic [2:0] new_state;

// 	logic [2:0] SNT = 2'b00;
// 	logic [2:0] LNT = 2'b01;
// 	logic [2:0] LT = 2'b10;
// 	logic [2:0] ST = 2'b11;

// 	logic [1:0] BT = 1'b1;
// 	logic [1:0] BNT = 1'b0;

// 	assign state = counter[gh ^ pc];
// 	assign predicted = predict_reg;
// 	assign counter[gh ^ pc] = new_state;

// 	always @(posedge reset) begin
// 		foreach(counter[i]) begin
// 			counter[i] <= 1;
// 		end
// 	end

// 	always @(pc, gh, n, counter[gh ^ pc]) begin
// 		predict_reg = counter[gh ^ pc][1];  
// 	end

// 	always @(posedge clk) begin
// 		if (update == 1)  begin
// 			if (state == SNT) begin
// 				if (actual == BT) begin
// 					counter[gh ^ pc] <= LNT;
// 				end else begin
// 					counter[gh ^ pc] <= SNT;
// 				end
// 			end else if (state == LNT) begin
// 				if (actual == BT) begin
// 					counter[gh ^ pc] <= LT;
// 				end else begin
// 					counter[gh ^ pc] <= SNT;
// 				end
// 			end else if (state == LT) begin
// 				if (actual == BT) begin
// 					counter[gh ^ pc] <= ST;
// 				end else begin
// 					counter[gh ^ pc] <= LNT;
// 				end
// 			end else if (state == ST) begin
// 				if (actual == BT) begin
// 					counter[gh ^ pc] <= ST;
// 				end else begin
// 					counter[gh ^ pc] <= LT;
// 				end
// 			end
// 		end	
// 	end

// endmodule
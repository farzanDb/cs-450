module counter(input clk, load, input [9:0] data, output tc);

	logic tc_reg;
	logic [9:0] count;

	initial begin
		tc_reg = 0;
		count = 0;
	end

	always @(count) begin
		if (count == 0) begin
			tc_reg = 1;
		end
	end

	always @(posedge clk) begin
		if (load == 1) begin
			tc_reg <= 0;
			count <= data;
		end else if (count != 0) begin			
			count <= count - 1;
		end
	end	

	assign tc = tc_reg;

endmodule

module counter_tb;
	logic clk, load;
	logic [9:0] data;
	logic tc;

	counter dut(.clk(clk), .load(load), .data(data), .tc(tc));

	initial begin
		clk = 0; forever #5 clk = ~clk;
	end

	initial begin
		data = 3; load = 1;
		#10 load = 0;
		$monitor("t=%2d tc=%1b", $time, tc);
		#40 data = 2; load = 1;
		#10 load = 0;
		#60 $stop;
	end

	initial $dumpvars(0, counter_tb);

endmodule

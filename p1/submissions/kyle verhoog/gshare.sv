module gshare(output reg [0:0] predicted, input [n-1:0] gh, pc,
	input actual, update, reset, clk);
	parameter n = 4;
	logic [1:0] counter [0:2**n-1];

  localparam SNT = 0, LNT = 1, LT = 2, ST = 3;
  integer idx;

  integer i = 0;
  always @(posedge clk or posedge reset) begin
    if (reset == 1) begin
      for (i = 0; i < $size(counter); i = i + 1) begin
        counter[i] <= LNT;
      end
    end

    idx = pc[n-1:0] ^ gh;

    predicted <= counter[pc[n-1:0] ^ gh][1:1];

    if (update == 1) begin
      if (counter[idx] == SNT && actual == 0) begin
        counter[idx] = SNT;
      end else if (counter[idx] == SNT && actual == 1) begin
        counter[idx] = LNT;
      end else if (counter[idx] == LNT && actual == 0) begin
        counter[idx] = SNT;
      end else if (counter[idx] == LNT && actual == 1) begin
        counter[idx] = LT;
      end else if (counter[idx] == LT && actual == 0) begin
        counter[idx] = LNT;
      end else if (counter[idx] == LT && actual == 1) begin
        counter[idx] = ST;
      end else if (counter[idx] == ST && actual == 0) begin
        counter[idx] = LT;
      end else if (counter[idx] == ST && actual == 1) begin
        counter[idx] = ST;
      end
    end
  end
endmodule


/* testbench module */
module gshare_tb;
	parameter m=1;
	logic [31:0] pc;
	logic [m-1:0] gr;
	logic actual, update, reset, clk;
	logic predicted;

	gshare #(.n(m)) dut(predicted, pc[m-1:0], gr, actual, update, reset, clk);

	initial begin
		clk = 0; forever #5 clk = ~clk;
	end

	initial $monitor("t=%2d: counter[0] %2b counter[1] %2b", $time,
		dut.counter[0], dut.counter[1]);
	initial begin
	reset = 1; update = 0; pc = 0; gr = 0;
	# 10 reset = 0;
	# 10 actual = 1; update = 1;
	# 10 actual = 1;
	# 10 actual = 0;
	# 10 actual = 0;
	# 10 actual = 0;
	# 10 gr = 1; update = 0;
	# 10 actual = 1; update = 1;
	# 10 actual = 1; update = 1;
	# 10 actual = 1; update = 1;
	# 10 update = 0;
	# 10 $stop;
	end

	initial $dumpvars(0, gshare_tb);

endmodule

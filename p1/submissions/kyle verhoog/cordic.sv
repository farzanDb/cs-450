module cordic(input clk, go, input [15:0] a,
	output logic done, output logic [15:0] sin, cos);
	logic [15:0] e [0:15];
	assign e[0] = 16'h3244;
	assign e[1] = 16'h1dac;
	assign e[2] = 16'h0fae;
	assign e[3] = 16'h07f5;
	assign e[4] = 16'h03ff;
	assign e[5] = 16'h0200;
	assign e[6] = 16'h0100;
	assign e[7] = 16'h0080;
	assign e[8] = 16'h0040;
	assign e[9] = 16'h0020;
	assign e[10] = 16'h0010;
	assign e[11] = 16'h0008;
	assign e[12] = 16'h0004;
	assign e[13] = 16'h0002;
	assign e[14] = 16'h0001;
	assign e[15] = 16'h0000;

	logic [15:0] x_0 = 16'h26dd;

  reg signed [15:0] x = 16'h26dd;
  reg signed [15:0] y = 0;
  reg signed [15:0] z;

  // Iteration being computed
  reg [4:0] iteration = 0;

  // Go state machine
  // goState=0 => go was not last seen (unknown)
  // goState=1 => go was last seen at 1
  // goState=2 => go was last seen at 0
  reg [1:0] goState = 0,
    nextGoState = 0;

  // Compute state machine
  // compute=0 => do not compute
  // compute=1 => ok to compute
  reg [0:0] compute = 0;

  integer index;

  always @(posedge clk) begin
    // State machine transitions
    if (goState == 0 && go == 0) begin
        nextGoState = 2;
    end else if (goState == 0 && go == 1) begin
        nextGoState = 1;
    end else if (goState == 1 && go == 0) begin
        nextGoState = 2;
        compute = 1; // trigger the compute to start
        done = 0;
        z = a;
    end else if (goState == 1 && go == 1) begin
        nextGoState = 1; // stay in state 1
    end else if (goState == 2 && go == 0) begin
        nextGoState = 2; // stay in state 2
    end else if (goState == 2 && go == 1) begin
        nextGoState = 1; // stay in state 2
    end
    goState <= nextGoState;

    // Compute
    if (compute == 1) begin
      // $display("t=%3d, i=%2d, x=%4h, y=%4h, z=%4h", $time, iteration, x, y, z);
      if (z < 0) begin
        // d = -1
        x <= x + $signed(y >>> iteration);
        y <= y - $signed(x >>> iteration);
        z <= z + e[iteration];
      end else begin
        // d = 1
        x <= x - $signed(y >>> iteration);
        y <= y + $signed(x >>> iteration);
        z <= z - e[iteration];
      end
      iteration <= iteration + 1;
    end

    if (iteration == 16) begin
      sin <= y;
      cos <= x;
      done <= 1;

      // reset state
      x <= 16'h26dd;
      y <= 0;
      z <= 0;
      compute <= 0;
      iteration <= 0;
    end else begin
      done <= 0;
    end
  end

endmodule

module cordic_tb;
	logic clk, go, done;
	logic [15:0] a, sin, cos;
	cordic dut(.clk(clk), .go(go), .a(a), .done(done), .sin(sin), .cos(cos));

	initial begin
		clk = 0; forever #5 clk = ~clk;
	end

	initial begin
		a = 16'h0; // angle = 0 rad
		go = 1;
		#10 go = 0;
		wait(done==1)
			// should display roughly sin=0000 and cos=4000 in hexadecimal
			$display("t=%3d sin=%4h cos=%4h", $time, sin, cos);

		#10 a = 16'h4000; // angle = 1 rad
		go = 1;
		#10 go = 0;
		wait(done==1)
			// should display roughly sin=35da and cos=2294 in hexadecimal
			$display("t=%3d sin=%4h cos=%4h", $time, sin, cos);

		#10 $stop;
	end

	initial $dumpvars(0,cordic_tb);

endmodule

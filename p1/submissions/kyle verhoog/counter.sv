module counter(input clk, load, input [9:0] data, output reg[1:0] tc);
reg [9:0] count;

always @(posedge clk) begin
  if (load) begin
    count <= data;
    tc <= 0;
  end else begin
    if (count > 0) begin
      count <= count - 1;
    end
    // if the count is about to be set to zero (available on the next
      // clock cycle, or is already zero then set tc high.
    if (count - 1 == 0 || count == 0) begin
      tc <= 1;
    end else begin
      tc <= 0;
    end
  end
end
endmodule

module counter_tb;
logic clk, load;
logic [9:0] data;

// note: I had to change this for it to compile
// logic tc;
wire [1:0] tc;

counter dut(.clk(clk), .load(load), .data(data), .tc(tc));

initial begin
  clk = 0; forever #5 clk = ~clk;
end

initial begin
  data = 3; load = 1;
  #10 load = 0;
  $monitor("t=%2d tc=%1b", $time, tc);
  #40 data = 2; load = 1;
  #10 load = 0;
  #60 $stop;
end

initial $dumpvars(0, counter_tb);

endmodule

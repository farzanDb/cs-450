module counter(input clk, load, input [9:0] data, output tc);
  reg [9:0] state;

  always @(posedge clk) begin
    if (load) begin
      // Load the data into the current state.
      state <= data;
    end else if (state > 0) begin
      // Count down.
      state <= state - 1;
    end
  end

  // Continuously set tc if 0 has been reached.
  assign tc = (state == 0);
endmodule

module counter_tb;
	logic clk, load;
	logic [9:0] data;
	logic tc;

	counter dut(.clk(clk), .load(load), .data(data), .tc(tc));

	initial begin
		clk = 0; forever #5 clk = ~clk;
	end

	initial begin
		data = 3; load = 1;
		#10 load = 0;
		$monitor("t=%2d tc=%1b", $time, tc);
		#40 data = 2; load = 1;
		#10 load = 0;
		#60 $stop;
	end

	initial $dumpvars(0, counter_tb);

endmodule

module cordic(input clk, go, input [15:0] a,
	output logic done, output logic [15:0] sin, cos);

  parameter fixed_pt = 14;

	logic [15:0] e [0:15];
	assign e[0] = 16'h3244;
	assign e[1] = 16'h1dac;
	assign e[2] = 16'h0fae;
	assign e[3] = 16'h07f5;
	assign e[4] = 16'h03ff;
	assign e[5] = 16'h0200;
	assign e[6] = 16'h0100;
	assign e[7] = 16'h0080;
	assign e[8] = 16'h0040;
	assign e[9] = 16'h0020;
	assign e[10] = 16'h0010;
	assign e[11] = 16'h0008;
	assign e[12] = 16'h0004;
	assign e[13] = 16'h0002;
	assign e[14] = 16'h0001;
	assign e[15] = 16'h0000;

	logic [15:0] x_0 = 16'h26dd;

  // The current x, y and z values.
	logic signed [15:0] x, y, z;

  // A counter, from 0 to 15. This represents what iteration we are on.
  logic [3:0] cur_step;

  wire signed [15:0] d_i;
  // two_pow_neg_i = 2^(-i), with 14 bits of precision.
  wire signed [15:0] two_pow_neg_i;
  // Registers to hold the new values of x and y while the old values
  // are being used for calculations.
  reg signed [32:0] tmp_x;
  reg signed [32:0] tmp_y;

  // Continuously set d_i and two_pow_neg_i.
  assign d_i = (z < 0 ? -1 : 1);
  assign two_pow_neg_i = 2 ** (fixed_pt-cur_step);

  always @(posedge clk) begin
    if (go) begin
      // Reset all values.
      done = 0;
      x = x_0;
      y = 0;
      z = a;
      cur_step = 0;
      done = 0;
      //$display("t=%3d, i=%2d, x=%4h y=%4h, z=%4h", $time, 0, x, y, z);
    end else if (!done) begin
      // Do a single step of the calculation.
      tmp_x = (y * d_i) * two_pow_neg_i;
      tmp_y = (x * d_i) * two_pow_neg_i;

      // Round tmp_x and tmp_y.
      if (tmp_x[fixed_pt-1:0] >= (1 << (fixed_pt - 1))) begin
        tmp_x += (1 << fixed_pt);
      end
      if (tmp_y[fixed_pt-1:0] >= (1 << (fixed_pt - 1))) begin
        tmp_y += (1 << fixed_pt);
      end
      // Since two 14-bit precision numbers were multiplied, tmp_x and tmp_y
      // are 28-bit precision numbers. They need to be shifted right to get
      // back to 14-bit precision numbers.
      x = x - (tmp_x >> fixed_pt);
      y = y + (tmp_y >> fixed_pt);
      z = z - d_i * e[cur_step];
      //$display("t=%3d, i=%2d, x=%4h y=%4h, z=%4h", $time, cur_step+1, x, y, z);

      if (cur_step == 15) begin
        // Done all 16 steps, so return result.
        done = 1;
        cos = x;
        sin = y;
      end else begin
        // Do next step next iteration.
        cur_step = cur_step + 1;
      end
   end
  end
endmodule

module cordic_tb;
	logic clk, go, done;
	logic [15:0] a, sin, cos;
	cordic dut(.clk(clk), .go(go), .a(a), .done(done), .sin(sin), .cos(cos));

	initial begin
		clk = 0; forever #5 clk = ~clk;
	end

	initial begin
		a = 16'h0; // angle = 0 rad
		go = 1;
		#10 go = 0;
		wait(done==1)
			// should display roughly sin=0000 and cos=4000 in hexadecimal
			$display("t=%3d sin=%4h cos=%4h", $time, sin, cos);

		#10 a = 16'h4000; // angle = 1 rad
		go = 1;
		#10 go = 0;
		wait(done==1)
			// should display roughly sin=35da and cos=2294 in hexadecimal
			$display("t=%3d sin=%4h cos=%4h", $time, sin, cos);

		#10 $stop;
	end

	initial $dumpvars(0,cordic_tb);

endmodule

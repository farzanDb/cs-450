module gshare(output predicted, input [n-1:0] gh, pc,
	input actual, update, reset, clk);
	parameter n = 4;

  // An array of counters, each holding the state of a DFA.
	logic [1:0] counter [0:2**n-1];

  // Store the xor of gh and pc, since it's used a lot.
  wire xor_pc;
  xor x1(xor_pc, gh, pc);

  // The prediction is based on the state of the DFA corresponding to the xor
  // of gh and pc.
  assign predicted = counter[xor_pc][1];

  integer i;
  always @(posedge clk, posedge reset) begin
    if (reset) begin
      // When reset goes positive, reset all counters.
      for (i = 0; i < 2 ** n; ++i) begin
        counter[i] <= 2'b01;
      end
    end else if (update) begin
      // Update the counter corresponding to the current input.
      case (counter[xor_pc])
        2'b00:
          if (actual) begin
            counter[xor_pc] <= 2'b01;
          end
        2'b01:
          if (actual) begin
            counter[xor_pc] <= 2'b10;
          end else begin
            counter[xor_pc] <= 2'b00;
          end
        2'b10:
          if (actual) begin
            counter[xor_pc] <= 2'b11;
          end else begin
            counter[xor_pc] <= 2'b01;
          end
        2'b11:
          if (!actual) begin
            counter[xor_pc] <= 2'b10;
          end
      endcase
    end
  end
endmodule


/* testbench module */
module gshare_tb;
	parameter m=1;
	logic [31:0] pc;
	logic [m-1:0] gr;
	logic actual, update, reset, clk;
	logic predicted;

	gshare #(.n(m)) dut(predicted, pc[m-1:0], gr, actual, update, reset, clk);

	initial begin
		clk = 0; forever #5 clk = ~clk;
	end

	initial $monitor("t=%2d: counter[0] %2b counter[1] %2b", $time,
		dut.counter[0], dut.counter[1]);
	initial begin
	reset = 1; update = 0; pc = 0; gr = 0;
	# 10 reset = 0;
	# 10 actual = 1; update = 1;
	# 10 actual = 1;
	# 10 actual = 0;
	# 10 actual = 0;
	# 10 actual = 0;
	# 10 gr = 1; update = 0;
	# 10 actual = 1; update = 1;
	# 10 actual = 1; update = 1;
	# 10 actual = 1; update = 1;
	# 10 update = 0;
	# 10 $stop;
	end

	initial $dumpvars(0, gshare_tb);

endmodule

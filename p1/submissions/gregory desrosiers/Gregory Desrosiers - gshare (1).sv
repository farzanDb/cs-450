
module gshare(output logic predicted, input [31:0] gh, pc,
	input actual, update, reset, clk);
	parameter n = 4;
	logic [1:0] counter [0:2**n-1];
	logic [n-1:0] indexForCounter;


	// Start out by setting the index for the counter array to 0 for consistent initialization.
	initial begin
		indexForCounter = 0;
	end

	// Any sequential action we do shall always be done at the rising edge of the clock.
	always @(posedge clk) begin

		// First, we compute the index of the counter element we want to assess.
		indexForCounter = gh[n-1:0] ^ pc[n-1:0];

		if (reset) begin
			// If the reset switch is activated, set all of the states in the counters to be the
			// Likely Not Taken state (which is 01 in binary).
			counter = '{2**n{1}};
		end else if (update) begin
			// Otherwise, we manipulate the states if and only if the update switch is activated,
			// and that the states themselves for the indexed counter is not at the boundaries.

			// Check if an actual branch has been taken or not.
			if (actual) begin
				// Update the state, but only if the state is not Strongly predicted taken
				// (as shown in the finite state machine)
				if (counter[indexForCounter] < 3)
					counter[indexForCounter] = counter[indexForCounter] + 1;
			end else begin
				// Update the state, but only if the state is not Strongly predicted Not Taken
				// (as shown in the finite state machine)
				if (counter[indexForCounter] > 0)
					counter[indexForCounter] = counter[indexForCounter] - 1;
			end
		end

		// Assign whether the branch is predicted or not by pulling the most significant bit
		// of the state the indexed counter stores.
		predicted = counter[indexForCounter][1:1];

	end

endmodule


/* testbench module */
module gshare_tb;
	parameter m=9;
	logic [31:0] pc;
	logic [m-1:0] gh;
	logic actual, update, reset, clk;
	logic predicted;

	gshare #(.n(m)) dut(predicted, gh, pc[m-1:0], actual, update, reset, clk);

	initial begin
		clk = 0; forever #5 clk = ~clk;
	end

	initial $monitor("t=%2d: counter[0] %2b counter[1] %2b", $time,
		dut.counter[0], dut.counter[1]);
	initial begin
		// Provided Test Case
		reset = 1; update = 0; pc = 0; gh = 0;
		# 10 reset = 0;
		# 10 actual = 1; update = 1;
		# 10 actual = 1;
		# 10 actual = 0;
		# 10 actual = 0;
		# 10 actual = 0;
		# 10 gh = 1; update = 0;
		# 10 actual = 1; update = 1;
		# 10 actual = 1; update = 1;
		# 10 actual = 1; update = 1;
		# 10 update = 0;
		# 10 $stop;


		// Additional Test Case by Gregory Desrosiers
		// 325 - 101000101
		// 165 - 010100101
		// Result - 111100000 = 480
		$monitor("t=%2d: counter[480] %2b counter[193] %2b", $time,
			dut.counter[480], dut.counter[193]);
		#10 reset = 1; update = 0; pc = 325; gh = 165;
		#10 reset = 0;
		#10 actual = 1; update = 1; // Go to state 10 (Predicted - 1)
		#10 actual = 0; // Go to state 01 (Predicted - 0)
		#10 actual = 0; // Go to state 00
		#10 actual = 0; // Stay at state 00
		#10 actual = 1; // Go to state 01
		#10 actual = 1; // Go to state 10 (Predicted - 1)
		#10 actual = 1; // Go to state 11
		#10 actual = 1; // Stay at state 11
		#10 update = 0;
		#10 pc = 46; gh = 239;
		// 46 - 00101110, 239 - 11101111 Result - 11000001 (193)
		#10 actual = 0; update = 1; // Go to state 00
		#10 actual = 1; // Go to state 01 (Predicted - 0)
		#10 actual = 1; // Go to state 10 (Predicted - 1)
		#10 actual = 1; // Go to state 11
		#10 actual = 0; // Go to state 10
		#10 actual = 1; // Go to state 11
		#10 update = 0;
		#10 $stop;


	end

	initial $dumpvars(0, gshare_tb);

endmodule

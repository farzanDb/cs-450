module counter(input clk, load, input [9:0] data, output logic tc);

	// Have a variable capable of both read and writes.
	logic [9:0] count;

	// Any sequential action we do shall always be done at the rising edge of the clock.
	always @(posedge clk) begin

		// On loading the counter, reset the current counter value to the starting value,
		// and reset the terminal count flag to 0. Upon letting the load go, decrement
		// the counter on every clock cycle until we reach 0.
		if (load) begin
			count = data;
			tc = 0;
		end else if (count > 0)
			count = count - 1;

		// When the counter value is no longer greater than zero, raise the terminal count flag.
		if (count == 0)
		  tc = 1;
	end

endmodule

module counter_tb;
	logic clk, load;
	logic [9:0] data;
	logic tc;

	counter dut(.clk(clk), .load(load), .data(data), .tc(tc));

	initial begin
		clk = 0; forever #5 clk = ~clk;
	end

	initial begin
		data = 3; load = 1;
		#10 load = 0;
		$monitor("t=%2d tc=%1b", $time, tc);
		#40 data = 2; load = 1;
		#10 load = 0;
		#60 $stop;

		// Custom Tests by Gregory Desrosiers
		// Test 1: Data is 10.
		#40 data = 10; load = 1;
		#10 load = 0;
		#100 $stop;

		// Test 2: Data is 100.
		#40 data = 100; load = 1;
		#10 load = 0;
		#990 $stop;

		// Test 3: Data is 1000.
		#40 data = 1000; load = 1;
		#10 load = 0;
		#9990 $stop;

	end

	initial $dumpvars(0, counter_tb);

endmodule

module cordic(input clk, go, input [15:0] a,
	output logic done, output logic [15:0] sin, cos);

	logic [15:0] e [0:15];
	assign e[0] = 16'h3244;
	assign e[1] = 16'h1dac;
	assign e[2] = 16'h0fae;
	assign e[3] = 16'h07f5;
	assign e[4] = 16'h03ff;
	assign e[5] = 16'h0200;
	assign e[6] = 16'h0100;
	assign e[7] = 16'h0080;
	assign e[8] = 16'h0040;
	assign e[9] = 16'h0020;
	assign e[10] = 16'h0010;
	assign e[11] = 16'h0008;
	assign e[12] = 16'h0004;
	assign e[13] = 16'h0002;
	assign e[14] = 16'h0001;
	assign e[15] = 16'h0000;

	logic [15:0] x_0 = 16'h26dd;

	logic signed [15:0] x, y, z;


	logic [3:0] iterationIndex;						// Four-bit iteration index.
	logic [1:0] d;
	logic sixteenIterationsComplete = 0;	//

	always @(posedge clk) begin
		if (go) begin
			iterationIndex = 0;
			x = x_0;
			y = 0;
			z = a;
			sixteenIterationsComplete = 0;
			done = 0;
			$display("Value of a: %d", a);
			$display("Initial value of z: %d", z);
		end else if (~sixteenIterationsComplete) begin

			//if (z < 0)
			//	d = -1;
			//else
			//	d = 1;

			if (z < 0) begin
				$display("z less than zero");
				x <= x + $signed(y >>> iterationIndex);
				y <= y - $signed(x >>> iterationIndex);
				z <= z + e[iterationIndex];
			end else begin
				$display("z greater or equal to zero");
				x <= x - $signed(y >>> iterationIndex);
				y <= y + $signed(x >>> iterationIndex);
				z <= z - e[iterationIndex];
			end

			if (iterationIndex < 15)
				iterationIndex = iterationIndex + 1;
			else begin
				cos = x;
				sin = y;
				done = 1;
				sixteenIterationsComplete = 1;
			end
		end
	end
endmodule

module cordic_tb;
	logic clk, go, done;
	logic [15:0] a, sin, cos;
	cordic dut(.clk(clk), .go(go), .a(a), .done(done), .sin(sin), .cos(cos));

	initial begin
		clk = 0; forever #5 clk = ~clk;
	end

	initial begin
		a = 16'h0; // angle = 0 rad
		go = 1;
		#10 go = 0;
		wait(done==1)
			// should display roughly sin=0000 and cos=4000 in hexadecimal
			$display("t=%3d sin=%4h cos=%4h", $time, sin, cos);

		#10 a = 16'h4000; // angle = 1 rad
		go = 1;
		#10 go = 0;
		wait(done==1)
			// should display roughly sin=35da and cos=2294 in hexadecimal
			$display("t=%3d sin=%4h cos=%4h", $time, sin, cos);

		#10 $stop;
	end

	initial $dumpvars(0,cordic_tb);

endmodule

module counter(input clk, load, input [9:0] data, output logic tc);
	logic [9:0] current;
	
	always @ (posedge clk) begin
		if(load==1) current=data;
		else current=current-1;
	end
	// Making the assumption that tc should only be 0 when the counter equals 0, 
	// not less than 0
	always @ (current) begin
		if(current==0) tc=1;
		else tc=0;
	end
endmodule

module counter_tb;
	logic clk, load;
	logic [9:0] data;
	logic tc;

	counter dut(.clk(clk), .load(load), .data(data), .tc(tc));

	initial begin
		clk = 0; forever #5 clk = ~clk;
	end

	initial begin
		data = 3; load = 1;
		#10 load = 0;
		$monitor("t=%2d tc=%1b", $time, tc);
		#40 data = 2; load = 1;
		#10 load = 0;
		#60 $stop;
	end

	initial $dumpvars(0, counter_tb);

endmodule

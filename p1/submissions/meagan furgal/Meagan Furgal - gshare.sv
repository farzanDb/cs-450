module gshare(output predicted, input [n-1:0] gh, pc,
	input actual, update, reset, clk);
	parameter n = 4;
	logic [1:0] counter [0:2**n-1];
	reg predict = 0;

	// your code here
	integer i = 0;
	integer counter_len = 2**(n-1);
	localparam SNT=2'b00, LNT=2'b01, LT=2'b10, ST=2'b11;
	logic [1:0] cur, next;
	
	always @ (posedge clk, posedge reset) begin
		if(reset) begin
			for(i = 0; i <= counter_len; ++i)
				counter[i] = LNT; 
			predict = 0;
		end
		else begin
			predict = next[1];
			counter[pc ^ gh] = next;	
		end
	end
	
	always @ (posedge clk) begin
		if(reset == 0 && update == 1) begin
			cur = counter[pc ^ gh];
			case (cur)
				SNT: if (actual) next = LNT;
					else next = SNT;
				LNT: if (actual) next = LT;
					else next = SNT;
				LT: if (actual) next = ST;
					else next = LNT;
				ST: if (actual) next = ST;
					else next = LT;
				default: next = 2'bxx;
			endcase
		end
	end
	assign predicted = predict;		
		
endmodule


/* testbench module */
module gshare_tb;
	parameter m=1;
	logic [31:0] pc;
	logic [m-1:0] gr;
	logic actual, update, reset, clk;
	logic predicted;

	gshare #(.n(m)) dut(predicted, pc[m-1:0], gr, actual, update, reset, clk);

	initial begin
		clk = 0; forever #5 clk = ~clk;
	end

	initial $monitor("t=%2d: counter[0] %2b counter[1] %2b", $time,
		dut.counter[0], dut.counter[1]);
	initial begin
	reset = 1; update = 0; pc = 0; gr = 0;
	# 10 reset = 0;
	# 10 actual = 1; update = 1;
	# 10 actual = 1;
	# 10 actual = 0;
	# 10 actual = 0;
	# 10 actual = 0;
	# 10 gr = 1; update = 0;
	# 10 actual = 1; update = 1;
	# 10 actual = 1; update = 1;
	# 10 actual = 1; update = 1;
	# 10 update = 0;
	# 10 $stop;
	end

	initial $dumpvars(0, gshare_tb);

endmodule

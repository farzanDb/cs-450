module counter(input clk, load, input [9:0] data, output tc);
	reg [9:0] data_;

	assign tc = data_ == 10'b0;

	always @ (posedge clk)
	begin
		if (load)
			data_ <= data;
		else begin
			if (data_ > 0)
				data_ <= data_ - 1;
		end
	end
endmodule

module counter_tb;
	logic clk, load;
	logic [9:0] data;
	logic tc;

	counter dut(.clk(clk), .load(load), .data(data), .tc(tc));

	initial begin
		clk = 0; forever #5 clk = ~clk;
	end

	initial begin
		data = 3; load = 1;
		#10 load = 0;
		$monitor("t=%2d tc=%1b", $time, tc);
		#40 data = 2; load = 1;
		#10 load = 0;
		#60 $stop;
	end

	initial $dumpvars(0, counter_tb);

endmodule

module gshare(output predicted, input [n-1:0] gh, pc,
	input actual, update, reset, clk);
	parameter n = 4;
	logic [1:0] counter [0:2**n-1];
	logic [n-1:0] index;

	assign index = pc ^ gh;
	assign predicted = counter[index][1];

	genvar i;
	generate
		for (i = 0; i < 2**n; i = i + 1) begin
			always @ (posedge reset) begin
				counter[i] <= 2'b01;
			end
		end
	endgenerate

	always @ (posedge clk)
	begin		
		if (update) begin
			if (counter[index] == 2'b00) begin
				if (actual)
					counter[index] <= 2'b01;
				else
					counter[index] <= 2'b00;
			end
			else if (counter[index] == 2'b01) begin
				if (actual)
					counter[index] <= 2'b10;
				else
					counter[index] <= 2'b00;
			end
			else if (counter[index] == 2'b10) begin
				if (actual)
					counter[index] <= 2'b11;
				else
					counter[index] <= 2'b01;
			end
			else if (counter[index] == 2'b11) begin
				if (actual)
					counter[index] <= 2'b11;
				else
					counter[index] <= 2'b10;
			end
		end
	end

endmodule


/* testbench module */
module gshare_tb;
	parameter m=1;
	logic [31:0] pc;
	logic [m-1:0] gr;
	logic actual, update, reset, clk;
	logic predicted;

	gshare #(.n(m)) dut(predicted, pc[m-1:0], gr, actual, update, reset, clk);

	initial begin
		clk = 0; forever #5 clk = ~clk;
	end

	initial $monitor("t=%2d: counter[0] %2b counter[1] %2b", $time,
		dut.counter[0], dut.counter[1]);
	initial begin
	reset = 1; update = 0; pc = 0; gr = 0;
	# 10 reset = 0;
	# 10 actual = 1; update = 1;
	# 10 actual = 1;
	# 10 actual = 0;
	# 10 actual = 0;
	# 10 actual = 0;
	# 10 gr = 1; update = 0;
	# 10 actual = 1; update = 1;
	# 10 actual = 1; update = 1;
	# 10 actual = 1; update = 1;
	# 10 update = 0;
	# 10 $stop;
	end

	initial $dumpvars(0, gshare_tb);

endmodule

module gshare(output predicted, input [n-1:0] gh, pc,
	input actual, update, reset, clk);
	parameter n = 4;
	logic [1:0] counter [0:2**n-1];

	// your code here
        logic predicted;
        integer i, idx;

        initial for (i=0;i<=2**n-1;++i) counter[i] = 1;

        always@(posedge clk) begin
                if (reset==1) begin
                        for (i=0;i<=2**n-1;++i) counter[i] = 1;
                end
                else if (update==1) begin
                        idx = pc ^ gh;
                        if (actual==1) begin
                                if(counter[idx] != 3)
                                        counter[idx] = counter[idx]+1;
                        end
                        else if (counter[idx] != 0)
                                counter[idx] = counter[idx]-1;
                end
                else
                begin
                        idx = pc ^ gh;
                        predicted = counter[idx][1];
                end
        end
endmodule


/* testbench module */
module gshare_tb;
	parameter m=1;
	logic [31:0] pc;
	logic [m-1:0] gr;
	logic actual, update, reset, clk;
	logic predicted;

	gshare #(.n(m)) dut(predicted, pc[m-1:0], gr, actual, update, reset, clk);

	initial begin
		clk = 0; forever #5 clk = ~clk;
	end

	initial $monitor("t=%2d: counter[0] %2b counter[1] %2b", $time,
		dut.counter[0], dut.counter[1]);
	initial begin
	reset = 1; update = 0; pc = 0; gr = 0;
	# 10 reset = 0;
	# 10 actual = 1; update = 1;
	# 10 actual = 1;
	# 10 actual = 0;
	# 10 actual = 0;
	# 10 actual = 0;
	# 10 gr = 1; update = 0;
	# 10 actual = 1; update = 1;
	# 10 actual = 1; update = 1;
	# 10 actual = 1; update = 1;
	# 10 update = 0;
	# 10 $stop;
	end

	initial $dumpvars(0, gshare_tb);

endmodule

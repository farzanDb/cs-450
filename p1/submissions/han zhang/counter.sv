module counter(input clk, load, input [9:0] data, output tc);
	reg [9:0] count;
	reg tc_r;
	// your code here

	always @(posedge clk) begin
		if (load) begin
			count <= data;
			if (data) tc_r <= 0;
			else tc_r <= 1;
		end
		else if (count) begin
			count <= count - 1;
			if (count==1) tc_r <= 1; // update in next cycle
		end
	end

	assign tc = tc_r;

endmodule

module counter_tb;
	logic clk, load;
	logic [9:0] data;
	logic tc;

	counter dut(.clk(clk), .load(load), .data(data), .tc(tc));

	initial begin
		clk = 0; forever #5 clk = ~clk;
	end

	initial begin
		data = 3; load = 1;
		#10 load = 0;
		$monitor("t=%2d tc=%1b", $time, tc);
		#40 data = 2; load = 1;
		#10 load = 0;
		#60 $stop;
	end

	initial $dumpvars(0, counter_tb);

endmodule

module counter(input clk, load, input [9:0] data, output tc);
	logic [9:0] counter_val = 0;
	logic ltc = 0;

	always @(posedge clk) begin
  		if(load) begin
			counter_val = data;
			ltc = 0;
		end
  		else begin
			counter_val = counter_val - 1;
			if(!counter_val) begin
				ltc = 1;
			end
		end
	end
	assign tc = ltc;
endmodule

module counter_tb;
	logic clk, load;
	logic [9:0] data;
	logic tc;

	counter dut(.clk(clk), .load(load), .data(data), .tc(tc));

	initial begin
		clk = 0; forever #5 clk = ~clk;
	end

	initial begin
		data = 3; load = 1;
		#10 load = 0;
		$monitor("t=%2d tc=%1b", $time, tc);
		#40 data = 2; load = 1;
		#10 load = 0;
		#60 $stop;
	end

	initial $dumpvars(0, counter_tb);

endmodule

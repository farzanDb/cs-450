module gshare(output predicted, input [n-1:0] gh, pc,
	input actual, update, reset, clk);
	parameter n = 4;
	logic [1:0] counter [0:2**n-1];

	integer i;
	integer indexer, carry_i;
	logic [1:0] pred;
	
	logic carry_flag = 0;
	logic signed [1:0] carry;

	// your code here

	initial begin
		for (i = 0; i < 2**n; i = i + 1) begin
			counter[i] = 2'b01;
		end
	end

	always @(posedge clk) begin
		// Carry update logic from last clock

		indexer = ((pc << (16 - n)) >> (16 - n)) ^ gh;

		if (reset) begin
			for (i = 0; i < 2**n; i = i + 1) begin
				counter[i] <= 2'b01;
			end
		end
		else if(update) begin
			carry_flag = 1;
			carry_i = indexer;
			carry = (actual)? 1 : -1;
			if(carry_flag) begin
				if ((counter[carry_i] < 2'b11 && carry > 0)
					|| (counter[carry_i] > 2'b00 && carry < 0))
					counter[carry_i] += carry;
				carry_flag = 0;
			end
		end
		
		pred = counter[indexer];
	end
	assign predicted = pred;
endmodule


/* testbench module */
module gshare_tb;
	parameter m=1;
	logic [31:0] pc;
	logic [m-1:0] gr;
	logic actual, update, reset, clk;
	logic predicted;

	gshare #(.n(m)) dut(predicted, pc[m-1:0], gr, actual, update, reset, clk);

	initial begin
		clk = 0; forever #5 clk = ~clk;
	end

	initial $monitor("t=%2d: counter[0] %2b counter[1] %2b", $time,
		dut.counter[0], dut.counter[1]);
	initial begin
	reset = 1; update = 0; pc = 0; gr = 0;
	# 10 reset = 0;
	# 10 actual = 1; update = 1;
	# 10 actual = 1;
	# 10 actual = 0;
	# 10 actual = 0;
	# 10 actual = 0;
	# 10 gr = 1; update = 0;
	# 10 actual = 1; update = 1;
	# 10 actual = 1; update = 1;
	# 10 actual = 1; update = 1;
	# 10 update = 0;
	# 10 $stop;
	end

	initial $dumpvars(0, gshare_tb);

endmodule

/*
 * Ahmed Al-Sudani (a2alsuda@edu.uwaterloo.ca)
 *
 * Sorry for submitting this part late--just fixed the last bug I had. I know
 * I used all my late-submission days for A2, so I understand if I lose all or
 * some marks here.
 */

// Opcodes
`define OP_ADDIU 6'b001001
`define OP_SW    6'b101011
`define OP_LI    6'b001001
`define OP_LW    6'b100011
`define OP_amb   6'b000000

// Functions
`define FN_ADD   6'b100000
`define FN_ADDU  6'b100001
`define FN_JR    6'b001000

module mips(
	          // port list
	          input               clk, reset,
	          output [31:0]       instr_addr,
	          input [31:0]        instr_in,
	          output [31:0]       data_addr,
	          input [31:0]        data_in,
	          output logic [31:0] data_out,
	          output              data_rd_wr);

	 // parameters overridden in testbench
	 parameter [31:0] pc_init = 0;
	 parameter [31:0] sp_init = 0;
	 parameter [31:0] ra_init = 0;


	 // IF signals
	 logic [31:0]                 pc;
	 assign instr_addr = pc;
	 logic [31:0]                 ir;

	 // ID signals
   logic [4:0]                  reg_rd_num0, reg_rd_num1, reg_rd_num2;
   wire [5:0]                   opcode, funct;
   wire [31:0]                  reg_rd_data0, reg_rd_data1;
   assign opcode = ir[31:26];
   assign funct = ir[5:0];
	 assign reg_rd_num0 = ir[25:21];
	 assign reg_rd_num1 = ir[20:16];
	 assign reg_rd_num2 = ir[15:11];

	 // EX signals
	 logic [31:0]                 a, b, sign_ext_imm;
	 logic [31:0]                 alu_out, mem_addr;
	 logic                        ex_mem_to_reg, ex_skip_write;

	 // ME signals
	 logic                        st_en;
	 assign data_out = b;
	 assign data_rd_wr = ~st_en;
	 assign data_addr = mem_addr;

	 // WD signals
   logic [4:0]                  reg_wr_num;
   logic [31:0]                 reg_wr_data;
   logic                        reg_wr_en;

	 enum                         { init, fetch, id, ex, me, wb } state;

	 // register file
   regfile #(.sp_init(sp_init), .ra_init(ra_init)) regs(
		                                                    .wr_num(reg_wr_num), .wr_data(reg_wr_data), .wr_en(reg_wr_en),
                                                        .rd0_num(reg_rd_num0), .rd0_data(reg_rd_data0),
		                                                    .rd1_num(reg_rd_num1), .rd1_data(reg_rd_data1),
                                                        .clk(clk), .reset(reset));

	 always @(posedge clk or posedge reset) begin
		  if(reset) begin
			   reg_wr_en <= 0;
			   pc <= pc_init;
			   state <= init;
         ex_skip_write <= 0;
		  end
		  else
			  case(state)
				  init: begin
					   // this state is needed since we have to wait for
					   // memory to produce the first instruction after reset
					   state <= fetch;
				  end
				  fetch: begin
					   ir <= instr_in;
					   pc <= pc + 4;
					   state <= id;
				  end
				  id: begin
					   a <= reg_rd_data0;
					   sign_ext_imm <= {{16{ir[15]}},ir[15:0]};
             // Default write register
					   reg_wr_num <= reg_rd_num1;
					   state <= ex;
             mem_addr <= $signed(reg_rd_data0 + {{16{ir[15]}},ir[15:0]});
				  end
				  ex: begin
             case(opcode)
               `OP_ADDIU: begin
					        alu_out <= a + sign_ext_imm;
					        // st_en <= 1;
               end
               `OP_amb: begin
                  case(funct)
                    `FN_ADD: begin
					             alu_out <= $signed(a + reg_rd_num1);
					             reg_wr_num <= reg_rd_num2;
                    end
                    `FN_ADDU: begin
					             alu_out <= a + reg_rd_num1;
					             reg_wr_num <= reg_rd_num2;
                    end
                    `FN_JR: begin
                       pc <= a;
                       ex_skip_write <= 1;
                    end
                    default: begin
                       $write("DECODE ERROR2");
                    end
                  endcase
               end
               `OP_SW: begin
                  st_en <= 1;
                  ex_skip_write <= 1;
                  b <= reg_rd_data1;
               end
               `OP_LI: begin
                  alu_out <= sign_ext_imm;
               end
               `OP_LW: begin
					        reg_wr_data <= data_in;
                  ex_mem_to_reg <= 1;
               end
               default: begin
                  $write("DECODE ERROR");
               end
             endcase
					   state <= me;
				  end
				  me: begin
					   st_en <= 0;
             ex_skip_write <= 0;
             ex_mem_to_reg <= 0;
					   reg_wr_en <= ~ex_skip_write;
             if (ex_mem_to_reg) begin
					      reg_wr_data <= data_in;
             end else begin
					      reg_wr_data <= alu_out;
             end
					   state <= wb;
				  end
				  wb: begin
					   reg_wr_en <= 0;
					   state <= fetch;
				  end
				  default: begin
					   reg_wr_en <= 0;
					   state <= fetch;
				  end
			  endcase
	 end

endmodule

// this is a mips module that can only execute the addiu instruction
module mips(
	// port list
	input clk, reset,										// Clock cycle and reset switch
	output [31:0] instr_addr,						// Instruction Address
	input [31:0] instr_in,							// Instruction Input
	output [31:0] data_addr,						// Data Address (For load / store mechanisms)
	output logic [31:0] data_in,								// Data Input (for store word)
	input logic [31:0] data_out,				// Data Output (for load word)
	output data_rd_wr);									// Data Read / Write Flag (whether to write or to read)

	// parameters overridden in testbench
	parameter [31:0] pc_init = 0;				// Program Counter initial value
	parameter [31:0] sp_init = 0;				// Stack Pointer initial value	(Register 29)
	parameter [31:0] ra_init = 0;				// Return Address initial value (Register 31)

	// IF signals
	logic [31:0] pc;										// Program Counter
	assign instr_addr = pc;							// Instruction address (to let the processor fetch the instructions from RAM, which include the length)
	logic [31:0] ir;										// Instruction Register

	// ID signals
  logic [4:0] reg_rd_num0, reg_rd_num1;		// Indexes of the registers for the operands.
  wire [31:0] reg_rd_data0, reg_rd_data1;	// The data these two operand registers hold.
	logic [5:0] opcode;											// The six-bit opcode that identifies which instruction the processor is to execute.
	logic [5:0] specialOperationCode;				// Six bits representing what operation to execute if the opcode at the front of the word is 0.
	assign reg_rd_num0 = ir[25:21];					// reg_rd_num0 is the first operand as part of the instruction word stored in the instruction register.
	assign reg_rd_num1 = ir[20:16];					// reg_rd_num1 is the second operand as part of the instruction word stored in the instruction register.
	assign opcode = ir[31:26];							// Always keep the opcode assigned with the six most significant bits of the instruction register.
	assign specialOperationCode = ir[5:0]; 	// Always keep the special operation code assigned with the six least significant bits of the instruction register.

	// EX signals
	logic [31:0] a, b, sign_ext_imm;	// Logic units for operands A and B needed for the arithmetic logic unit
	logic [15:0] loadAndStoreOffset;	// The Offset value associated with the load / store word instruction (must be added)
	logic [31:0] effectiveAddress;		// The effective address for the load / store operation to take effect.
	assign data_addr = effectiveAddress;
	logic [31:0] alu_out;							// The output of the arithmetic logic unit.

	// ME signals
	logic st_en;											// Store enable flag (this sets whether to store or read data from memory)
	assign data_in = b;							// Data_out holds an intermediate result which can be returned for debugging.
	assign data_rd_wr = ~st_en;				// Flag as to whether read or write the data to and from the register.

	// WD signals
  logic [4:0] reg_wr_num;					// Which register to write the data to.
  logic [31:0] reg_wr_data;				// The actual value that will be written to the register.
  logic reg_wr_en;								// Whether writing to the register is enabled or not (must be on for only one cycle.)

	enum { init, fetch, id, ex, me, wb } state;

	// register file
	// This file handles the exchange of data for inputs and outputs.
	// wr_num - Index of the register to write the data to.
	// wr_data - Actual data to be written.
	// wr_en - Write enable (whether to allow the processor to do the write)
	// rd0_num - Index of the register to be used as the first of the two operands.
	// rd0_data - Actual value of the register as the first operand.
	// rd1_num - Index of the register to be used as the second of the two operands.
	// rd1_data - Actual value of the register as the second operand.
    regfile #(.sp_init(sp_init), .ra_init(ra_init)) regs(
		.wr_num(reg_wr_num), .wr_data(reg_wr_data), .wr_en(reg_wr_en),
        .rd0_num(reg_rd_num0), .rd0_data(reg_rd_data0),
		.rd1_num(reg_rd_num1), .rd1_data(reg_rd_data1),
        .clk(clk), .reset(reset));

	// Simple Instructions:
	// Simply extend the finite state machine to handle all the other assembly instructions
	// that exist in SimpleAdd.s. Use the SimpleAdd.dmp and MIPS references for help and to see
	// how to break the instruction word down into its corresponding parts.


	// ADDITIONAL NOTES:
	// For store and load word instructions, do not worry about the data that the frame pointer register holds.
	// What matters is, the data is stored and loaded correctly (if time persists at the end, though, see if a test can be
	// done where the value of the frame pointer is set incorrectly)
	// The frame pointer is always set with don't cares.


	always @(posedge clk or posedge reset) begin
		if(reset) begin
			reg_wr_en <= 0;
			pc <= pc_init;
			state <= init;
		end
		else
			case(state)
				init: begin
					// this state is needed since we have to wait for
					// memory to produce the first instruction after reset
					state <= fetch;
				end
				fetch: begin
					ir <= instr_in;		// Get the instruction word.
					pc <= pc + 4;			// Increment the program counter by four bytes.
					state <= id;			// Go to the Instruction Decode stage.
				end
				id: begin
					if (opcode == 6'b001001)
						begin	// ADDIU Instruction
							// The code below is for the ADDIU instruction only.
							a <= reg_rd_data0;	// Get the value of the first operand register and have it as operand A for the Execute stage.
							sign_ext_imm <= {{16{ir[15]}},ir[15:0]};		// Perform the sign extend by concatenating the last 16 bits of the instruction register with the most significant bit.
							$display("\nADDIU instruction decoded\n");
						end
					else if (opcode == 6'b101011 || opcode == 6'b100011) // SW Instruction, or LW Instruction
						begin

							if (opcode == 6'b101011)
								$display("\nSW instruction decoded");
							else
								$display("\nLW Instruction decoded");

							// Let a hold the data of the register that holds the base location for storing the word. In other words, reg_rd_data0 holds the base address.
							// In the case of $fp,20($sp), reg_rd_num0 = 29 (stack pointer), and reg_rd_num1 = 30 (stack frame pointer).
							a <= reg_rd_data0;

							// Let b be the actual data we want to write to memory. (Only needed if the operation is SW.)
							if (opcode == 6'b101011)
								b <= reg_rd_data1;

							// Get the offset from the least 16 significant digits of the instruction word, performing sign extension at the same time.
							loadAndStoreOffset <= {{16{ir[15]}},ir[15:0]};
						end
				//	else if () // LW Instruction
					//	begin
					//	end
					else if (opcode == 6'b000000) // Special instructions with opcode in the six least significant bits.
						begin
							$display("Special operation detected");

							if (specialOperationCode == 6'b100001)	// ADDU Instruction
								begin
									$display("ADDU instruction decoded");
									a <= reg_rd_data0;
									b <= reg_rd_data1;
								end
							else if (specialOperationCode == 6'b001000)	// JR Instruction (J in the case of SimpleAdd.s)
								begin
									$display("JR Instruction decoded");
									a <= reg_rd_data0;
								end
						end
					state <= ex;			// Go to the Execution stage.
				end
				ex: begin
					if (opcode == 6'b001001)
						alu_out <= a + sign_ext_imm;					// Perform the add. (In this case, the ADDIU Instruction)
					// st_en <= 1; //uncomment to enable store to mem (Needed for the Store Word instruction.)
					else if (opcode == 6'b101011 || opcode == 6'b100011) // SW or LW Instruction
						begin
							alu_out = loadAndStoreOffset + a;
							if (alu_out[1] | alu_out[0] == 1'b1)	// Making sure that the effective address is naturally aligned
								$error("Bad address in data/stack read = %8h", alu_out);	// MUST BE TESTED TO SEE HOW THE SIMULATION REACTS
							effectiveAddress = alu_out;
						end
					else if (opcode == 6'b000000) // Special instructions with opcode in the six least significant bits.
						begin
							if (specialOperationCode == 6'b100001)	// ADDU Instruction
								begin
									alu_out <= a + b;
								end
							else if (specialOperationCode == 6'b001000)	// JR Instruction (J in the case of SimpleAdd.s)
								begin
									alu_out = a;
									if (alu_out[1] | alu_out[0] == 1'b1)	// Making sure that the effective address is naturally aligned
										$error("Bad address in jump register = %8h", alu_out);
									effectiveAddress = alu_out;
									pc = effectiveAddress;
								end
						end

					state <= me;
				end
				me: begin
					if (opcode == 6'b001001) // ADDIU Instruction
						begin
							reg_wr_en <= 1;						// Raise the Write Enable flag to have the processor set the flip flops in the destination register
							reg_wr_num <= ir[20:16];	// Pull five bits for the second operand as the register to store some data. (destination register)
							reg_wr_data <= alu_out;		// Assign the output of the ALU to the logic unit that will pass the data to the destination register.
						end
					else if (opcode == 6'b101011) // SW Instruction
						begin
							st_en <= 1;												// Raise the Store Flag to begin the write process.
							//data_addr <= effectiveAddress;	// Set the data address for the store taking place to be the effective address.
							//data_in <= b;									// Set the data word to be stored.
						end
					else if (opcode == 6'b100011) // LW Instruction
						begin
							st_en <= 0;
							reg_wr_en <= 1;
							reg_wr_num <= ir[20:16];
							//data_addr = effectiveAddress;
							reg_wr_data = data_out;
						end
					else if (opcode == 6'b000000) // Special instructions with opcode in the six least significant bits.
						begin
							if (specialOperationCode == 6'b100001)	// ADDU Instruction
								begin
									reg_wr_en <= 1;
									reg_wr_num <= ir[15:11];
									reg_wr_data <= alu_out;
								end
							else if (specialOperationCode == 6'b001000)	// JR Instruction (J in the case of SimpleAdd.s)
								begin
								end
						end

					state <= wb;							// Go to the Writeback stage.
				end
				wb: begin
					
					st_en <= 0;								// Drop the Store Enable flag to prevent any stores from taking place.
					reg_wr_en <= 0;						// Drop the Write Enable to prevent data from being exchanged on the next clock cycle.
					state <= fetch;						// Go back to the fetch stage to handle the next instruction.
				end
				default: begin
					reg_wr_en <= 0;
					state <= fetch;
				end
			endcase
	end

endmodule

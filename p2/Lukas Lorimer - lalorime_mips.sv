// this is a mips module that can only execute the addiu instruction
module mips(
	// port list
	input clk, reset,
	output [31:0] instr_addr,
	input [31:0] instr_in,
	output [31:0] data_addr,
	input [31:0] data_in,
	output logic [31:0] data_out,
	output data_rd_wr);

	// parameters overridden in testbench
	parameter [31:0] pc_init = 0;
	parameter [31:0] sp_init = 0;
	parameter [31:0] ra_init = 0;

	// IF signals
	logic [31:0] pc;
	assign instr_addr = pc;
	logic [31:0] ir;

	// ID signals
  wire [5:0] id_opcode;
  wire [4:0] id_rs, id_rt, id_rd;
  wire [15:0] id_offset;
  wire [5:0] id_func;

  assign id_opcode = ir[31:26];
  // R-type.
  assign id_rs = ir[25:21];
  assign id_rt = ir[20:16];
  assign id_rd = ir[15:11];
  assign id_func = ir[5:0];
  // I-type.
  assign id_offset = ir[15:0];

  wire [31:0] id_rs_data, id_rt_data;
  wire [31:0] id_imm;
  assign id_imm = {{16{ir[15]}},ir[15:0]};

	// EX signals
	wire [31:0] alu_arg1;
  wire [31:0] alu_arg2;
	logic [31:0] alu_out;

	enum { register, imm } alu_arg2_src;

  assign alu_arg1 = id_rs_data;
  assign alu_arg2 = (alu_arg2_src == register ? id_rt_data : id_imm);

	// ME signals
  logic should_write_mem;
	logic st_en;
	assign data_addr = alu_out;
	assign data_out = id_rt_data;
	assign data_rd_wr = ~st_en;

	// WD signals
  logic should_write_back;
  logic reg_wr_en;
  logic [4:0] wd_dest;

  enum { alu_result, memory_load } wd_data_source;
  wire [31:0] wd_data;
  assign wd_data = (wd_data_source == alu_result) ? alu_out : data_in;

  // For the test bench.
  wire [4:0] reg_wr_num;
  assign reg_wr_num = wd_dest;
  wire [31:0] reg_wr_data;
  assign reg_wr_data = wd_data;

	enum { init, fetch, id, ex, me, wb } state;

	// register file
    regfile #(.sp_init(sp_init), .ra_init(ra_init)) regs(
		.wr_num(wd_dest), .wr_data(wd_data), .wr_en(reg_wr_en),
        .rd0_num(id_rs), .rd0_data(id_rs_data),
		.rd1_num(id_rt), .rd1_data(id_rt_data),
        .clk(clk), .reset(reset));

	always @(posedge clk or posedge reset) begin
		if(reset) begin
			reg_wr_en <= 0;
      should_write_back <= 0;
      wd_dest <= 0;
			pc <= pc_init;
			state <= init;
		end
		else
			case(state)
				init: begin
					// this state is needed since we have to wait for
					// memory to produce the first instruction after reset
					state <= fetch;
				end
				fetch: begin
					ir <= instr_in;
					pc <= pc + 4;
					state <= id;
				end
				id: begin
          should_write_mem <= 0;
          should_write_back <= 1;
          wd_data_source <= alu_result;
          wd_dest <= id_rd;
					state <= ex;

          case (id_opcode)
            6'b000000: begin
              // Special
              case (id_func)
                6'b100001: begin
                  // ADDU
                  alu_arg2_src <= register;
                end
                6'b001000: begin
                  // JR
                  // TODO: Is there a nicer way to do this?
                  pc <= id_rs_data;
                  state <= fetch;
                  should_write_back <= 0;
                end
              endcase
            end
            6'b001001: begin
              // ADDIU
              alu_arg2_src <= imm;
              wd_dest <= id_rt;
            end
            6'b100011: begin
              // LW
              alu_arg2_src <= imm;
              wd_dest <= id_rt;
              wd_data_source <= memory_load;
            end
            6'b101011: begin
              // SW
					    should_write_mem <= 1;
              should_write_back <= 0;

              alu_arg2_src <= imm;
            end
          endcase
				end
				ex: begin
          st_en <= should_write_mem;
          alu_out <= alu_arg1 + alu_arg2;
					state <= me;
				end
				me: begin
					st_en <= 0;

					reg_wr_en <= should_write_back;
					state <= wb;
				end
				wb: begin
					reg_wr_en <= 0;
					state <= fetch;
				end
				default: begin
					reg_wr_en <= 0;
					state <= fetch;
				end
			endcase
	end

endmodule

// this is a mips module that can only execute the addiu instruction
module mips(
	// port list
	input clk, reset,
	output [31:0] instr_addr,
	input [31:0] instr_in,
	output logic [31:0] data_addr, // change to logic?
	input [31:0] data_in,
	output logic [31:0] data_out,
	output data_rd_wr);

	// parameters overridden in testbench
	parameter [31:0] pc_init = 0;
	parameter [31:0] sp_init = 0;
	parameter [31:0] ra_init = 0;

	// IF signals
	logic [31:0] pc;
	assign instr_addr = pc;
	logic [31:0] ir;

	// ID signals
    logic [4:0] reg_rd_num0, reg_rd_num1;
    wire [31:0] reg_rd_data0, reg_rd_data1;
	assign reg_rd_num0 = ir[25:21];
	assign reg_rd_num1 = ir[20:16];

	// EX signals
	logic [31:0] a, b, sign_ext_imm;
	logic [31:0] alu_out;

	// ME signals
	logic st_en;
	assign data_out = b;
	assign data_rd_wr = ~st_en;

	// WD signals
    logic [4:0] reg_wr_num;
    logic [31:0] reg_wr_data;
    logic reg_wr_en;

	enum { init, fetch, id, ex, me, wb } state;

	// register file
    regfile #(.sp_init(sp_init), .ra_init(ra_init)) regs(
		.wr_num(reg_wr_num), .wr_data(reg_wr_data), .wr_en(reg_wr_en),
        .rd0_num(reg_rd_num0), .rd0_data(reg_rd_data0),
		.rd1_num(reg_rd_num1), .rd1_data(reg_rd_data1),
        .clk(clk), .reset(reset));

	always @(posedge clk or posedge reset) begin
		if(reset) begin
			reg_wr_en <= 0;
			pc <= pc_init;
			state <= init;
		end
		else
			case(state)
				init: begin
					// this state is needed since we have to wait for
					// memory to produce the first instruction after reset
					state <= fetch;
				end
				fetch: begin
					ir <= instr_in;
					pc <= pc + 4;
					state <= id;
				end
				id: begin
					a <= reg_rd_data0;

                    // different section
                    
                        // I think we have to assign b here before next step if required
                        
                    if ((ir[31:26] == 6'b000000) && (ir[5:0] == 6'b001000)) begin
                        // modifying instruction reg
                        ir[25:21] <= 5'b11111;
                    end
                    else begin
                        b <= reg_rd_data1;
                    end

                    // =================

					sign_ext_imm <= {{16{ir[15]}},ir[15:0]};
					state <= ex;
				end
				ex: begin           // here we handle the various instructions
                    if (ir[31:26] == 6'b001001) begin // already done
					    alu_out <= a + sign_ext_imm;
					    // st_en <= 1; uncomment to enable store to mem
                        st_en <= 0;
                    end
                    else if(ir[31:26] == 6'b101011) begin
                        data_addr <= a + sign_ext_imm;
                        st_en <= 1;
                        b <= reg_rd_data1;
                    end
                    else if (ir[31:26] == 6'b100011) begin
                        data_addr <= a + sign_ext_imm;
                        st_en <= 0;
                    end
                    else if (ir[31:26] == 6'b000000) begin // still nee dto handle pseudo instructions
                        if (ir[5:0] == 6'b100001) begin
                            alu_out <= a + b;
                            st_en <= 0;
                        end
                        else if (ir[5:0] == 6'b001000) begin
                            a <= reg_rd_data0;
                            st_en <= 0;
                        end
                        else if (ir[25:0] == 0) begin
                            st_en <= 0;
                        end
                    end

                    // finally update state
					state <= me;
				end
				me: begin   // access memory if needed so need to enable reg_wr
                    if (ir[31:26] == 6'b001001) begin
                        reg_wr_en <= 1;
                        reg_wr_num <= ir[20:16];
                        reg_wr_data <= alu_out;
                    end
                    else if (ir[31:26] == 6'b101011) begin
                        reg_wr_en <= 0;
                        reg_wr_num <= 0;
                    end
                    else if (ir[31:26] == 6'b100011) begin
                        reg_wr_en <= 1;
                        reg_wr_num <= ir[20:16];
                    end
                    else if (ir[31:26] == 6'b000000) begin // still nee dto handle pseudo instructions
                        // not positive about the rest of this stage
                        if (ir[5:0] == 6'b100001) begin
                            reg_wr_en <= 1;
                            reg_wr_num <= ir[15:11];
                        end
                        else if (ir[5:0] == 6'b001000) begin
                            reg_wr_en <= 1;
                            reg_wr_num <= 0;
                            pc <= a;
                        end
                        else if (ir[25:0] == 0) begin
                            reg_wr_en <= 0;
                            reg_wr_num <= 0;
                        end
                    end

                    // move state
                    st_en <= 0;
					state <= wb;
				end
				wb: begin
					reg_wr_en <= 0;
					state <= fetch;
				end
				default: begin
					reg_wr_en <= 0;
					state <= fetch;
				end
			endcase
	end

endmodule

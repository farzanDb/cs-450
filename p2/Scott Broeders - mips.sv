// this is a mips module that can only execute the addiu instruction
module mips(
	// port list
	input clk, reset,
	output [31:0] instr_addr,
	input [31:0] instr_in,
	output [31:0] data_addr,
	input [31:0] data_in,
	output logic [31:0] data_out,
	output data_rd_wr);

	// parameters overridden in testbench
	parameter [31:0] pc_init = 0;
	parameter [31:0] sp_init = 0;
	parameter [31:0] ra_init = 0;

	// IF signals
	logic [31:0] pc;
	assign instr_addr = pc;
	logic [31:0] ir;

	// ID signals
    logic [4:0] reg_rd_num0, reg_rd_num1;
    wire [31:0] reg_rd_data0, reg_rd_data1;
	assign reg_rd_num0 = ir[25:21];
	assign reg_rd_num1 = ir[20:16];

	// EX signals
	logic [31:0] a, b, sign_ext_imm;
	logic [31:0] alu_out;

	// ME signals
	logic st_en;
	assign data_addr = alu_out;
	assign data_out = b;
	assign data_rd_wr = ~st_en;

	// WD signals
    logic [4:0] reg_wr_num;
    wire [31:0] reg_wr_data;
	assign reg_wr_data = (ir[31:26] != 6'b100011) ? alu_out : data_in; // data_in only if lw
    logic reg_wr_en;

	enum { init, fetch, id, ex, me, wb } state;

	// register file
    regfile #(.sp_init(sp_init), .ra_init(ra_init)) regs(
		.wr_num(reg_wr_num), .wr_data(reg_wr_data), .wr_en(reg_wr_en),
        .rd0_num(reg_rd_num0), .rd0_data(reg_rd_data0),
		.rd1_num(reg_rd_num1), .rd1_data(reg_rd_data1),
        .clk(clk), .reset(reset));

	always @(posedge clk or posedge reset) begin
		if(reset) begin
			reg_wr_en <= 0;
			pc <= pc_init;
			state <= init;
		end
		else
			case(state)
				init: begin
					// this state is needed since we have to wait for
					// memory to produce the first instruction after reset
					state <= fetch;
				end
				fetch: begin
					ir <= instr_in;
					pc <= pc + 4;
					state <= id;
				end
				id: begin
					a <= reg_rd_data0;
					sign_ext_imm <= {{16{ir[15]}},ir[15:0]};
					state <= ex;
					if (ir[31:26] == 6'b000000 & ir[20:0] == 21'b000000000000000001000) begin // jr
						pc <= reg_rd_data0;
					end
				end
				ex: begin
					alu_out <= a + sign_ext_imm;
					if (ir[31:26] == 6'b101011) begin // sw
						b <= reg_rd_data1;
						st_en <= 1;
					end
					if (ir[31:26] == 6'b000000) begin // addu
						alu_out <= a + reg_rd_data1;
					end
					state <= me;
				end
				me: begin
					st_en <= 0;
					if (ir[31:26] != 6'b101011 & !(ir[31:26] == 6'b000000 & ir[20:0] == 21'b000000000000000001000)) begin // not sw or jr
						reg_wr_en <= 1;
						reg_wr_num <= ir[20:16];
						if (ir[31:26] == 6'b000000) begin // addu
							reg_wr_num <= ir[15:11];
						end
					end
					state <= wb;
				end
				wb: begin
					reg_wr_en <= 0;
					state <= fetch;
				end
				default: begin
					reg_wr_en <= 0;
					state <= fetch;
				end
			endcase
	end

endmodule

// this is a mips module that can only execute the addiu instruction
module mips(
	// port list
	input clk, reset,
	output [31:0] instr_addr,
	input [31:0] instr_in,
	output [31:0] data_addr,
	input [31:0] data_in,
	output logic [31:0] data_out,
	output data_rd_wr);

	// parameters overridden in testbench
	parameter [31:0] pc_init = 0;
	parameter [31:0] sp_init = 0;
	parameter [31:0] ra_init = 0;

	// IF signals
	logic [31:0] pc;
	assign instr_addr = pc;
	logic [31:0] ir;

	// ID signals
    logic [4:0] reg_rd_num0, reg_rd_num1;
    wire [31:0] reg_rd_data0, reg_rd_data1;
	assign reg_rd_num0 = ir[25:21];
	assign reg_rd_num1 = ir[20:16];

	// EX signals
	logic [31:0] a, b, sign_ext_imm;
	logic [31:0] alu_out;

	// ME signals
	logic st_en;
	assign data_out = b;
	assign data_addr = alu_out;
	assign data_rd_wr = ~st_en;

	// WD signals
    logic [4:0] reg_wr_num;
    logic [31:0] reg_wr_data;
    logic reg_wr_en;

	enum { init, fetch, id, ex, me, wb } state;
	// li is just addiu, move is just addu
	enum { addiu, sw, lw, addu, jr, nop } opcode;

	// register file
    regfile #(.sp_init(sp_init), .ra_init(ra_init)) regs(
		.wr_num(reg_wr_num), .wr_data(reg_wr_data), .wr_en(reg_wr_en),
        .rd0_num(reg_rd_num0), .rd0_data(reg_rd_data0),
		.rd1_num(reg_rd_num1), .rd1_data(reg_rd_data1),
        .clk(clk), .reset(reset));

	always @(posedge clk or posedge reset) begin
		if(reset) begin
			reg_wr_en <= 0;
			pc <= pc_init;
			state <= init;
		end
		else
			case(state)
				init: begin
					// this state is needed since we have to wait for
					// memory to produce the first instruction after reset
					state <= fetch;
				end
				fetch: begin
					ir <= instr_in;
					pc <= pc + 4;
					state <= id;
					//$write("fe phase pc %8h: data_addr %8h data_in %8d\n", pc, data_addr , data_in);
				end
				id: begin
					// Identify what the instruction is.
					case(ir[31:26])
						6'b101011: begin opcode <= sw; end
						6'b001001: opcode <= addiu;
						6'b100011: opcode <= lw;
						// Special opcode
						6'b000000: begin
							case(ir[5:0])
								6'b100001: opcode <= addu;
								6'b001000: opcode <= jr;
								6'b000000: opcode <= nop;
							endcase
						end
					endcase // ir[31:26]
					//$write("id phase pc %8h: data_addr %8h data_in %8d\n", pc, data_addr , data_in);
					a <= reg_rd_data0;
					b <= reg_rd_data1;
					sign_ext_imm <= {{16{ir[15]}},ir[15:0]};
					state <= ex;
				end
				ex: begin
					//$write("ex phase pc %8h: data_addr %8h data_in %8d\n", pc, data_addr , data_in);
					// Calculate output
					case (opcode)
						addu: alu_out <= a + b;
						jr: pc <= a;
						default: alu_out <= a + sign_ext_imm;
					endcase
					// Check if memory is being stored.
					case(opcode) // Store to mem or not
						sw: st_en <= 1;
						default: st_en <= 0;
					endcase
					state <= me;
				end
				me: begin
					//$write("me phase pc %8h: data_addr %8h data_in %8d\n", pc, data_addr , data_in);
					// Turn off store to mem now.
					st_en <= 0;
					case(opcode) // Store to reg or not
						addiu: begin 
							reg_wr_en <= 1;
							reg_wr_num <= ir[20:16];
						end
						addu: begin
							reg_wr_en <= 1;
							reg_wr_num <= ir[15:11];
						end
						default: reg_wr_en <= 0;
					endcase
					reg_wr_data <= alu_out;
					state <= wb;
				end
				wb: begin
					//$write("wb phase pc %8h: data_addr %8h data_in %8d\n", pc, data_addr , data_in);
					// Reading from memory?
					case (opcode)
						lw: begin 
							reg_wr_data <= data_in;
							reg_wr_en <= 1;
							reg_wr_num <= ir[20:16];
						end
						default: reg_wr_en <= 0;
					endcase
					state <= fetch;
				end
				default: begin
					reg_wr_en <= 0;
					state <= fetch;
				end
			endcase // state
	end

endmodule

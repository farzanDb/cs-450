// this is a mips module that can only execute the addiu instruction
module mips(
	// port list
	input clk, reset,
	output [31:0] instr_addr,
	input [31:0] instr_in,
	output [31:0] data_addr,
	input [31:0] data_in,
	output logic [31:0] data_out,
	output data_rd_wr);

	// parameters overridden in testbench
	parameter [31:0] pc_init = 0;
	parameter [31:0] sp_init = 0;
	parameter [31:0] ra_init = 0;

	// IF signals
	logic [31:0] pc;
	assign instr_addr = pc;
	logic pc_jmp;
	logic [31:0] ir;

	// ID signals
    logic [4:0] reg_rd_num0, reg_rd_num1;
    wire [31:0] reg_rd_data0, reg_rd_data1;
	assign reg_rd_num0 = ir[25:21];
	assign reg_rd_num1 = ir[20:16]; 

	// EX signals
	logic [31:0] a, b, sign_ext_imm;
	logic [31:0] alu_out;

	// ME signals
	logic st_en;
	assign data_out = b;
	assign data_rd_wr = ~st_en;
	assign data_addr = alu_out;

	// WD signals
    logic [4:0] reg_wr_num;
    logic [31:0] reg_wr_data;
    logic reg_wr_en;
    logic mem_to_reg;

    // choose what is written back to register based on mem_to_reg signal
    always @(mem_to_reg, data_in, alu_out) begin
    	if (mem_to_reg == 1) begin 
    		reg_wr_data <= data_in;
    	end else begin
    		reg_wr_data <= alu_out;
    	end
    end

	enum { init, fetch, id, ex, me, wb } state;

	// register file
    regfile #(.sp_init(sp_init), .ra_init(ra_init)) regs(
		.wr_num(reg_wr_num), .wr_data(reg_wr_data), .wr_en(reg_wr_en),
        .rd0_num(reg_rd_num0), .rd0_data(reg_rd_data0),
		.rd1_num(reg_rd_num1), .rd1_data(reg_rd_data1),
        .clk(clk), .reset(reset));

	always @(posedge clk or posedge reset) begin
		if(reset) begin
			reg_wr_en <= 0;
			mem_to_reg <= 0;
			pc_jmp <= 0;
			st_en <= 0;
			pc <= pc_init;
			state <= init;
		end
		else
			case(state)
				init: begin
					// this state is needed since we have to wait for
					// memory to produce the first instruction after reset
					state <= fetch;
				end
				fetch: begin
					ir <= instr_in;
					if (pc_jmp == 1) begin // jump
						pc <= a;
					end else begin
						pc <= pc + 4;
					end
					state <= id;
				end
				id: begin
					pc_jmp <= 0;
					a <= reg_rd_data0;
					b <= reg_rd_data1;
					sign_ext_imm <= {{16{ir[15]}},ir[15:0]};
					state <= ex;
				end
				ex: begin
					if (ir[31:26] == 6'b001001 || ir[31:26] == 6'b100011) begin // addiu or lw
						alu_out <= a + sign_ext_imm;
					end else if (ir[31:26] == 6'b000000 && ir[5:0] == 6'b100001) begin // addu
						alu_out <= a + b;
					end else if (ir[31:26] == 6'b101011) begin // sw
						alu_out <= a + sign_ext_imm;
						st_en <= 1;	
					end
					state <= me;
				end
				me: begin
					if (ir[31:26] == 6'b100011) begin // lw
						reg_wr_en <= 1;
						reg_wr_num <= ir[20:16];
						mem_to_reg <= 1;
					end else if (ir[31:26] == 6'b001001) begin // addiu	
						reg_wr_en <= 1;
						reg_wr_num <= ir[20:16];
					end else if (ir[31:26] == 6'b000000 && ir[5:0] == 6'b100001) begin // addu
						reg_wr_en <= 1;
						reg_wr_num <= ir[15:11];
					end
					st_en <= 0;
					state <= wb;
				end
				wb: begin
					if (ir[31:26] == 6'b000000 && ir[5:0] == 6'b001000) begin // jr
						pc_jmp = 1;
					end
					mem_to_reg <= 0;
					reg_wr_en <= 0;
					state <= fetch;
				end
				default: begin
					reg_wr_en <= 0;
					state <= fetch;
				end
			endcase
	end

endmodule

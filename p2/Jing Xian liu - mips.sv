// this is a mips module that can only execute the addiu instruction
module mips(
	// port list
	input clk, reset,
	output [31:0] instr_addr,
	input [31:0] instr_in,
	output [31:0] data_addr,
	input [31:0] data_in,
	output logic [31:0] data_out,
	output data_rd_wr);

	// parameters overridden in testbench
	parameter [31:0] pc_init = 0;
	parameter [31:0] sp_init = 0;
	parameter [31:0] ra_init = 0;

	//logic [31:0] data_addr;
	//logic [31:0] data_out;
	//logic data_rd_wr;

	// IF signals
	logic [31:0] pc;
	assign instr_addr = pc;
	logic [31:0] ir;

	// ID signals
    logic [4:0] reg_rd_num0, reg_rd_num1;
	logic [5:0] instruction_type, special_instruction;
    wire [31:0] reg_rd_data0, reg_rd_data1;
	assign instruction_type = ir[31:26];
	assign reg_rd_num0 = ir[25:21];
	assign reg_rd_num1 = ir[20:16];
	assign special_instruction = ir[5:0];

	// EX signals
	logic [31:0] a, b, sign_ext_imm;
	logic [31:0] alu_out;

	// ME signals
	logic st_en;
	logic [31:0] addr;
	assign data_out = b;
	assign data_rd_wr = ~st_en;
	assign data_addr = addr;

	// WD signals
    logic [4:0] reg_wr_num;
    logic [31:0] reg_wr_data;
    logic reg_wr_en;

	enum { init, fetch, id, ex, me, wb } state;

	// register file
    regfile #(.sp_init(sp_init), .ra_init(ra_init)) regs(
		.wr_num(reg_wr_num), .wr_data(reg_wr_data), .wr_en(reg_wr_en),
        .rd0_num(reg_rd_num0), .rd0_data(reg_rd_data0),
		.rd1_num(reg_rd_num1), .rd1_data(reg_rd_data1),
        .clk(clk), .reset(reset));

	always @(posedge clk or posedge reset) begin
		if(reset) begin
			reg_wr_en <= 0;
			pc <= pc_init;
			state <= init;
		end
		else
			case(state)
				init: begin
					// this state is needed since we have to wait for
					// memory to produce the first instruction after reset
					state <= fetch;
				end
				fetch: begin
					reg_wr_en <= 0;
					ir <= instr_in;
					pc <= pc + 4;
					state <= id;
					st_en <= 0;
				end
				id: begin
					case(instruction_type)
						6'b001001: begin		//addiu
							a <= reg_rd_data0;
							sign_ext_imm <= {{16{ir[15]}},ir[15:0]};
						end
						6'b000000: begin		//addu
							a <= reg_rd_data0;
							b <= reg_rd_data1;
							//	$write ("    address to load from [%8h] [%8h]", reg_rd_data0, reg_rd_data1 );
						end
						6'b101011: begin		//sw
							a <= reg_rd_data0;
							b <= reg_rd_data1;
							sign_ext_imm <= {{16{ir[15]}},ir[15:0]};
						end
						6'b100011: begin		//lw
							b <= reg_rd_data1;
							addr = reg_rd_data0 +  {{16{ir[15]}},ir[15:0]};
						end
					endcase
					state <= ex;
				end
				ex: begin
					case(instruction_type)
						6'b001001: begin		//addiu
							alu_out <= a + sign_ext_imm;
						end
						6'b000000: begin		//addu or jr
							case(special_instruction )
								6'b100001: begin		//addu
									alu_out <= a + b;
								//	$write ("    address to load from [%8h] [%8h]", a, b );
								end
								6'b001000: begin		//jr
									pc <= a;
								end
							endcase
						end
						6'b101011: begin		//sw
							addr = a + sign_ext_imm;
							st_en <= 1;
								//$write ("    address to load from [%8h]", b);
						end
						6'b100011: begin		//lw
							reg_wr_num <= ir[20:16];
						//	addr = a + sign_ext_imm;
						end
					endcase
					state <= me;

				end
				me: begin
					reg_wr_en = 1;
					case(instruction_type)
						6'b001001: begin		//addiu
							reg_wr_num <= ir[20:16];
							//st_en <= 0;
							reg_wr_data <= alu_out;
						end
						6'b000000: begin		//addu or jr
							case(special_instruction )
								6'b100001: begin		//addu
									reg_wr_num <= ir[15:11];
							//		st_en <= 0;
									reg_wr_data <= alu_out;
								end
								6'b001000: begin	//jr
									reg_wr_en <= 0;
								end
							endcase
						end
						6'b101011: begin		//sw
							reg_wr_en <= 0;
						end
						6'b100011: begin		//lw
							reg_wr_data = data_in;
						//	$write ("    address to load from [%8h]", reg_wr_data);
						end
					endcase
					state <= wb;
				end
				wb: begin
					state <= fetch;
				end
				default: begin
					//reg_wr_en <= 0;
					state <= fetch;
				end
			endcase
	end

endmodule

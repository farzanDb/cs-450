module mips(
	// port list
	input clk, reset,
	output [31:0] instr_addr,
	input [31:0] instr_in,
	output logic [31:0] data_addr,
	input [31:0] data_in,
	output logic [31:0] data_out,
	output logic data_rd_wr);

	// parameters overridden in testbench
	parameter [31:0] pc_init = 0;
	parameter [31:0] sp_init = 0;
	parameter [31:0] ra_init = 0;

	// IF signals
	logic [31:0] pc;
	assign instr_addr = pc;
	logic [31:0] ir;

	// ID signals
	logic [4:0] reg_rd_num0, reg_rd_num1;
	wire [31:0] reg_rd_data0, reg_rd_data1;
	assign reg_rd_num0 = ir[25:21];
	assign reg_rd_num1 = ir[20:16];

	// EX signals
	logic [31:0] a, b, sign_ext_imm;
	logic [31:0] alu_out;

	// ME signals
	logic st_en;
	assign data_out = b;
	assign data_rd_wr = ~st_en;

	// WD signals
    	logic [4:0] reg_wr_num;
    	logic [31:0] reg_wr_data;
    	logic reg_wr_en;

	logic [5:0] add_start = 6'b000000;
	logic [5:0] add_end = 6'b100001;
	logic [5:0] addiu_start = 6'b001001;
	logic [5:0] sw_start = 6'b101011;
	logic [5:0] lw_start = 6'b100011;
	logic [5:0] jump_start = 6'b000010;
	logic [5:0] nop_start = 6'b000000;
	logic [5:0] nop_end = 6'b000000;
	
	logic [5:0] start_bits;
	logic [5:0] end_bits;

	reg[31:0] data;

	enum { init, fetch, id, ex, me, wb } state;
	enum { addiu, addu, jump, lw, sw, nop } op; 
	// move is a psuedo-instruction for addiu, li is a pseudo-instruction for addu

	// register file
    	regfile #(.sp_init(sp_init), .ra_init(ra_init)) regs(
		.wr_num(reg_wr_num), .wr_data(reg_wr_data), .wr_en(reg_wr_en),
        .rd0_num(reg_rd_num0), .rd0_data(reg_rd_data0),
		.rd1_num(reg_rd_num1), .rd1_data(reg_rd_data1),
        .clk(clk), .reset(reset));

	always @(posedge clk or posedge reset) begin
		if(reset) begin
			reg_wr_en <= 0;
			pc <= pc_init;
			state <= init;
		end
		else begin
			case(state)
				init: begin
					// this state is needed since we have to wait for
					// memory to produce the first instruction after reset
					state <= fetch;
				end
				fetch: begin
					ir <= instr_in;
					pc <= pc + 4;
					state <= id;
				end
				id: begin
					start_bits = ir[31:26];
					end_bits = ir[5:0];
					if (start_bits===addiu_start) op <= addiu;
					else if (start_bits===add_start && end_bits===add_end) op <= addu;
					else if (start_bits===lw_start) op <= lw;
					else if (start_bits===sw_start) op <= sw;
					else if (start_bits===jump_start) op <= jump;
					else op <= nop;					
					
					case(op)
						addiu: begin		
							a <= reg_rd_data0; // rs
							sign_ext_imm <= {{16{ir[15]}},ir[15:0]};
							state <= ex;
						end
						addu: begin
							a <= reg_rd_data0; // rs
							b <= reg_rd_data1; // rt
							state <= ex;
						end
						jump: begin
							pc <= {pc[31:28], ir[25:0] << 2};
							state <= fetch;
						end
						lw: begin
							a <= reg_rd_data0; // base
							sign_ext_imm <= {{16{ir[15]}},ir[15:0]};
							state <= ex;
						end
						sw: begin
							a <= reg_rd_data0; // base
							sign_ext_imm <= {{16{ir[15]}},ir[15:0]};
							state <= ex;
						end
						nop: begin
							a <= reg_rd_data0; // rs
							b <= reg_rd_data1; // rt
							state <= ex;
						end
						default: begin
							state <= fetch;
						end
					endcase
				end
				ex: begin
					case(op)
						addiu: begin		
							alu_out <= a + sign_ext_imm;
							state <= me;							
						end
						addu: begin
							alu_out <= a + b;
							state <= me;							
						end
						lw: begin
							data_addr <= a + sign_ext_imm;
							state <= me;
						end
						sw: begin
							data_addr <= a + sign_ext_imm;
							st_en <= 1;
							state <= me;
						end
						nop: begin
							alu_out <= a;
							state <= me;
						end
						default: begin
							state <= fetch;
						end
					endcase
				end
				me: begin
					case(op)
						addiu: begin
							st_en <= 0;
							reg_wr_en <= 1;
							reg_wr_num <= ir[20:16];
							reg_wr_data <= alu_out;
							state <= wb;					
						end
						addu: begin
							st_en <= 0;
							reg_wr_en <= 1;
							reg_wr_num <= ir[15:11];
							reg_wr_data <= alu_out;
							state <= wb;					
						end
						lw: begin
							st_en <= 0;
							reg_wr_en <= 1;
							reg_wr_num <= ir[20:16];
							state <= wb;
						end
						sw: begin
							st_en <= 1;
							reg_wr_en <= 0;
							reg_wr_num <= ir[25:21];
							data <= reg_wr_data;
							state <= wb;
						end
						nop: begin
							st_en <= 0;
							reg_wr_en <= 1;
							reg_wr_num <= ir[25:21];
							reg_wr_data <= alu_out;
							state <= wb;
						end
						default: begin
							state <= fetch;
						end
					endcase
				end
				wb: begin
					case(op)
						lw: begin
							reg_wr_data <= data_in;
							reg_wr_en <= 0;
							state <= fetch;
						end
						sw: begin							
							reg_wr_en <= 0;
							state <= fetch;
						end
						default: begin
							reg_wr_en <= 0;
							state <= fetch;
						end
					endcase
				end
				default: begin
					reg_wr_en <= 0;
					state <= fetch;
				end
			endcase
		end
		//assign data_out = data;
		// couldn't figure out an error stemming from this preventing compilation, so sw doesn't work
	end
endmodule

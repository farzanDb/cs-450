`include "booth.v"
module booth_test;

reg  clk, rst;
reg [1:0] inp;
wire [1:0] outp;
reg[4:0] sequence1;
reg[4:0] sequence2;
reg[10:0] a, s, p, carry;
integer i;

booth dut( clk, rst, inp, outp);

initial
begin

   clk = 0;
        rst = 1;
        sequence1 = 5'b00011;
        sequence2 = 5'b00011;
        
        a = sequence1;
        a = a << 6;
        s = 0 - sequence1;
        s = s << 6;
        p = sequence2;
        p = p << 1;
        inp = p[1:0];

        $display("%d x%d:", sequence1, sequence2, "\n");
   #5 rst = 0;

   for( i = 0; i < 5; i = i + 1)
   begin
      #2 clk = 1;
      #2 clk = 0;

      begin
      if (inp == 2'b01) begin
         p = p + a;

      end
      else if (inp == 2'b10) begin
         p = p + s;

      end
      end
      carry = p >> 10;

      p = p >> 1;
      if ( carry ) begin
         p = p + (carry << 10);
      end
      
      inp = p[1:0];
      $display(i + 1, " Partial sum: %b", p, " Window %b\n", inp);
   end
   $display("Result = %b = %d", p >> 1, p >> 1);
end

endmodule

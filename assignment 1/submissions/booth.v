module Booth_Multiplication(output reg[1:0] out, input [0:0] in, input clk, input reset);


reg[1:0] s;
reg[1:0] nextS;


initial begin
	s = 2'b00;
end

always@ (posedge clk) begin
	if (reset) begin
		s <= 2'b00;
	end else begin
		s <= nextS;
	end
end

always@(*) begin
	nextS = s;
	out = 0;
case(s)

	2'b00: if (in == 1) begin
		nextS = 2'b10;
		out = 2'b11;
		end
		else begin
		nextS = 2'b00;
		out = 0;
		end

	2'b10: if (in ==1) begin
		nextS = 2'b11;
		out = 0;
		end
		else begin
		nextS = 2'b01;
		out = 1;
		end

	2'b11: if (in == 1) begin
		nextS = 2'b11;
		out = 0;
		end
		else begin
		nextS = 2'b01;
		out = 1;
		end

	2'b01: if (in == 1) begin
		nextS = 2'b10;
		out = 2'b11;
		end
		else begin
		nextS = 2'b00;
		out = 0;
		end

endcase
end
endmodule

module booth( clk, rst, inp, outp);

   input clk, rst;
   input [1:0] inp;
   output [1:0] outp;

   reg [1:0] state;
   reg [1:0] outp;

   always @( posedge clk, posedge rst ) begin
   if( rst ) begin
       state <= 2'b00;
   end
   else  begin
       case( state )
       2'b00: begin
            if( inp ) begin
               state <= inp;
               outp  <= 2'b11;
            end
            else begin
                state <= 2'b00;
                outp <= 2'b00;
            end
       end

       2'b01: begin
            if( inp ) begin
               state <= inp;
               outp  <= 2'b11;
            end
            else begin
                state <= inp;
                outp <= 2'b00;
            end
       end

       2'b10: begin
            if( inp ) begin
                state <= inp;
                outp  <= 2'b00;
            end
            else begin
               state <= inp;
               outp <= 2'b01;
            end
       end

       2'b11: begin
            if( inp ) begin
                state <= inp;
                outp  <= 2'b00;
            end
            else begin
               state <= inp;
               outp <= 2'b01;
            end
       end
     endcase
   end
end

endmodule
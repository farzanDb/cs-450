`timescale 1ns/1ns
`include "booth.v"

module Test_Booth;

	reg clk;
	reg reset;
	reg in;
	wire[1:0] out;

Booth_Multiplication booth (
	.out(out),
	.in(in),
	.clk(clk),
	.reset(reset)
);

	always
		begin	
			clk <= 0; #5;
			clk <= 1; #5;
		end

	initial
		begin 
			//$dumpfile("test.vcd");
			//$dumpvars(0, out, clk, in);
			//$monitor("output is %b", out);
			reset <= 1; #10;

			reset <= 0; in <= 0; #10;

			in <= 1; #10;
			in <= 1; #10;
			in <= 1; #10;
			in <= 0; #10;
			in <= 1; #10;
			in <= 0; #10;
			in <= 0; #10;
			$finish;
		end

endmodule

// CS450 Assignment 1
// Kyle Verhoog (20563051)
//
// Performs testing of the Booth FSM defined in booth.v.
// This is done by performing each transition and manually
// verifying the results in gtkwave.
module booth_test;
  reg clk, reset, in;

  wire [1:0] val;

  booth_fsm c(.out(val), .clk(clk), .reset(reset), .in(in));

  initial begin
    clk = 0; forever #10 clk = ~clk;
  end

  // perform each transition in the FSM
  initial begin
    #0 reset = 1;
    #0 reset = 0;
    #0 in = 1;
    // initially it should be in state 0
    // and outputting 2 because in=1
    // setting input=1 should set the nextState
    // and output should be 2
    // 10s
    //  - clock posedge!
    //  - now in state 10 outputting 0
    // 30s
    //  - clock posedge!
    //  - now in state 11 outputting 0
    // 50s
    //  - still in state 11 outputting 0
    // 55s
    //  - FSM is in state 11, in=0 so
    //    output should now be 1
    #55 in = 0;
    // 70s
    //  - clock posedge!
    //  - now in state 01 with in=0 outputting 0
    // 90s
    //  - clock posedge!
    //  - now in state 00 outputting 0
    // 110s
    //  - clock posedge!
    //  - still in state 00 outputting 0
    // 120s
    //   - in=1
    //   - output now 2
    #65 in = 1;
    // 130s
    //  - clock posedge!
    //  - now in state 10 outputting 0
    // 140s
    //  - in=0
    //  - output now 1
    #20 in = 0;
    // 150s
    //  - clock posedge!
    //  - now in state 01, in=0 so outputting 0
    // 160s
    //  - in=1
    //  - state is 01 so output now 2
    #20 in = 1;
    // 170s
    //  - clock posedge!
    //  - now in state 10, in=1 so output is 0
    // 180s
    //  - reset=1
    //  - so state should be 00, in=1 so output
    //    should be 2
    #20 reset = 1;
    // 185s
    //   - in=0, in state 00 so output should be 0
    #5 in = 0;
    #5 $stop;
  end

  initial $dumpvars(0, booth_test);
endmodule

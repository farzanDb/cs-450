// CS450 Assignment 1
// Kyle Verhoog (20563051)

// An FSM for Booth multiplication operations.
module booth_fsm(output reg[1:0] out, input clk, reset, in);
  localparam A=0, B=1, C=2, D=3;
  reg [1:0] state,
    nextState;

  always @(*) begin
    nextState = state;
    out = 0;
    case ({state, in})
      // (A=00)
      // |---(1/2)--->(C=10)
      // +---(0/0)--->(A=00)
      {A, 1'b1}:
      begin
        nextState = C;
        out = 2;
      end
      {A, 1'b0}:
      begin
        nextState = A;
        out = 0;
      end
      // (B=01)
      // |---(1/2)--->(C=10)
      // +---(0/0)--->(A=00)
      {B, 1'b1}:
      begin
        nextState = C;
        out = 2;
      end
      {B, 1'b0}:
      begin
        nextState = A;
        out = 0;
      end
      // (C=10)
      // |---(1/0)--->(D=11)
      // +---(0/1)--->(B=01)
      {C, 1'b1}:
      begin
        nextState = D;
        out = 0;
      end
      {C, 1'b0}:
      begin
        nextState = B;
        out = 1;
      end
      // (D=11)
      // |---(1/0)--->(D=11)
      // +---(0/1)--->(B=01)
      // D: if (in) begin
      {D, 1'b1}:
      begin
        nextState = D;
        out = 0;
      end
      {D, 1'b0}:
      begin
        nextState = B;
        out = 1;
      end
      default : begin
        nextState = A;
        out = 0;
      end
    endcase
  end

  always @(posedge clk, posedge reset) begin
    if (reset) begin
      state <= A;
    end else begin
      state <= nextState;
    end
  end
endmodule
